@extends('front.layouts.blog')

@section('content')
    <section id="blog" class="single_blog_wrapper">
        <div class="blog_with_wrapper p-8">
            <div class="container">
                <h1 class="title">
                    {{ $post["title_$lang"] }}
                </h1>
                <div class="row">
                    <div class="main_image col-lg-3 col-md-4 col-sm-5 col-12">
                        @if (isset($post['image']))
                            <img class="img-fluid" src="{{ asset("images/posts/{$post['image']}") }}">
                        @else
                            <img class="img-fluid" src="{{ asset("images/posts/default.png") }}">
                        @endif
                    </div>
                    <div class="main_content col-lg-9 col-md-8 col-sm-7 col-12">
                        {!! $post["body_$lang"] !!}
                    </div>
                </div>
                <div class="row mb-2 mt-2">
                    <button type="button" class="consult_btn_blue"
                            data-toggle="modal" data-target="#formModal">
                        {{ $text['form']['btn_consult'] }}</button>
                </div>
            </div>
        </div>
    </section>
@endsection