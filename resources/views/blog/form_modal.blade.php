<!-- Modal -->
<div class="modal fade formModal" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('images/logo.png') }}" alt="">
                <h4 class="modal-title text-center title_white" id="formModalLabel">{{ $text['form']['title'] }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form autocomplete="off" id="modal_form"
                              class="form_apply">
                            <!--<div class="form-group">
                                <input type="text" name="name" autocomplete="false"
                                       placeholder="{{ $text['form']['name'] }}" class="form-control">
                                <input type="text" name="phone" autocomplete="false"
                                       placeholder="{{ $text['form']['phone'] }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <textarea style="width: 90%; margin: 0 auto; resize: none;"
                                        placeholder="{{ $text['form']['problem'] }}"
                                          name="problem" class="form-control" rows="3"></textarea>
                            </div>-->
                            <div class="form-group">
                                <button class="send_btn" type="button">
                                    <i class="fa fa-send"></i>
                                    {{ $text['form']['btn_send'] }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
