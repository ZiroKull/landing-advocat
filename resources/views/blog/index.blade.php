@extends('front.layouts.blog')

@section('content')
    @if($posts)
        <section id="blog" class="blog_with">
            <div class="blog_with_wrapper p-8">
                <div class="container">
                    <h1 class="text-center title">{{ $text['help_with']['title2'] }}:</h1>
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-lg-4 col-8 portfolio-item mb-3">
                                <div class="card h-100">
                                    <a target="_blank" href="{{ url('/post/')}}/{{ $post['slug'] }}">
                                        @if (isset($post['image']))
                                            <img class="card-img-top" src="{{ asset("images/posts/{$post['image']}") }}">
                                        @else
                                            <img class="card-img-top" src="{{ asset("images/posts/default.png") }}">
                                        @endif
                                    </a>
                                    <div class="card-body">
                                        <h2 class="card-title text-uppercase">
                                            <a target="_blank" href="{{ url('/post/')}}/{{ $post['slug'] }}">
                                                {{ $post["title_$lang"] }}
                                            </a>
                                        </h2>
                                        <p class="card-text">
                                            {!! substr($post["body_$lang"], 0, 180) . ' ...' !!}
                                        </p>
                                        <a target="_blank" href="{{ url('/post/')}}/{{ $post['slug'] }}"
                                           class="more_btn_blue">{{ $text['blog']['more'] }}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-12">
                            {{ $posts->links() }}
                        </div>
                    </div>
                    <div class="row">
                            <button type="button" class="consult_btn_blue"
                                    data-toggle="modal" data-target="#formModal">
                                {{ $text['form']['btn_consult'] }}</button>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection