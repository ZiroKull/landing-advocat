<!-- Modal -->
<div class="modal fade formModal item_modal" id="corectModal" tabindex="-1" role="dialog"
     aria-labelledby="aviaLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('images/logo.webp') }}" alt="">
                <h4 class="modal-title text-center title_white"
                    id="aviaLabel">Corectitudine</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul style="margin-left: 50px">
                                <li>cinste</li>
                                <li>onestitate</li>
                                <li>neprihănire</li>
                                <li>dreptate</li>
                                <li>justețe</li>
                                <li>obiectivitate</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <form autocomplete="off" class="form_apply">
                            <div class="form-group">
                                <input type="text" name="name" autocomplete="false"
                                       placeholder="{{ $text['form']['name'] }}" class="form-control">
                                <input type="text" name="phone" autocomplete="false"
                                       placeholder="{{ $text['form']['phone'] }}" class="form-control">
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input terms" name="terms">
                                    <label class="form-check-label" for="terms">{{ $text['form']['terms1'] }}
                                        <a href="{{ url('/terms/')}}" target="_blank">{{ $text['form']['terms2'] }}</a>
                                    </label>
                                </div>
                                <div class="form-group mt-5">
                                    <button class="send_btn" type="button">
                                        <i class="fa fa-send"></i>
                                        {{ $text['form']['btn_send'] }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>