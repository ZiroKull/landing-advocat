<!doctype html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    {{--    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':--}}
    {{--                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],--}}
    {{--            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=--}}
    {{--            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);--}}
    {{--        })(window,document,'script','dataLayer','GTM-W3Q2HS8');</script>--}}
    {{--    <!-- End Google Tag Manager -->--}}

    {{--    <!-- Global site tag (gtag.js) - Google Analytics -->--}}
    {{--    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77915149-2"></script>--}}
    {{--    <script>--}}
    {{--        window.dataLayer = window.dataLayer || [];--}}
    {{--        function gtag(){dataLayer.push(arguments);}--}}
    {{--        gtag('js', new Date());--}}

    {{--        gtag('config', 'UA-77915149-2');--}}
    {{--    </script>--}}

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>Avocat Moldova</title>

    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    <!--Tawk.to live chat-->
    {{--TODO: Uncomment on production--}}
    {{--    <script type="text/javascript">--}}
    {{--        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();--}}
    {{--        (function(){--}}
    {{--            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];--}}
    {{--            s1.async=true;--}}
    {{--            s1.src='https://embed.tawk.to/5b8acefeafc2c34e96e8208c/default';--}}
    {{--            s1.charset='UTF-8';--}}
    {{--            s1.setAttribute('crossorigin','*');--}}
    {{--            s0.parentNode.insertBefore(s1,s0);--}}
    {{--        })();--}}
    {{--    </script>--}}
</head>
<body>
{{--<!-- Google Tag Manager (noscript) -->--}}
{{--<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3Q2HS8"--}}
{{--                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>--}}
{{--<!-- End Google Tag Manager (noscript) -->--}}

@include('front.form_modal')

{{--LITTLE NAV FOR MOBILE--}}
<div class="little_nav_bar">
    <ul class="second_nav">
        <li><a href="{{ url('/') }}">{{ $text['menu']['home'] }}</a></li>
        <li><a href="#blog">Blog</a></li>
        <li><a href="#contacts">{{ $text['menu']['contacts'] }}</a></li>
    </ul>
    <div class="close_little_btn"><i class="fa fa-angle-up"></i></div>
</div>

<div id="main_wrapper">
    <div class="container-fluid">

        <section class="top_header">
            <div class="blue_line"></div>
            <div class="grey_border"></div>
        </section>

        {{--CLASSICAL NAVIGATION FOR PC--}}
        <section class="bottom_header">
            <div class="blue_box"></div>
            <div class="container">
                <div class="top_part">
                    <div class="row">
                        <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 offset-xl-7 offset-lg-7 offset-md-0">
                            <ul class="social_links list-unstyled">
                                <li>
                                    <a target="_blank"
                                       href="http://www.facebook.com/pages/Cabinetul-avocatului-Veaceslav-Spalatu/786247954759235?timeline_context_item_type=intro_card_work&timeline_context_item_source=100001098787548&fref=tag">
                                        <img src="{{ asset('images/icons/fb2.webp') }}" alt="">
                                    </a>
                                </li>
                                <li><a target="_blank"
                                       href="http://ok.ru/profile/575963072404">
                                        <img src="{{ asset('images/icons/ok2.webp') }}" alt="">
                                    </a></li>
                                <li><a target="_blank"
                                       href="http://www.linkedin.com/in/avocat-moldova-88148116a/">
                                        <img src="{{ asset('images/icons/link2.webp') }}" alt="">
                                    </a></li>
                            </ul>

                            <ul class="contacts list-unstyled">
                                <li class="text-right">
                                    <a href="tel:+373 79998902">
                                        <img src="{{ asset('images/icons/phone.webp') }}" alt="">
                                        <span>+373 79998902</span></a>
                                </li>
                                <li class="text-right">
                                    <a href="mailto:info@avocat-moldova.md">
                                        <img src="{{ asset('images/icons/mail.webp') }}" alt="">
                                        <span>info@avocat-moldova.md</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row" id="second_menu_row">
                        <div class="col-md-2 col-sm-2 col-1">
                            <img class="main_logo" src="{{ asset('images/logo.webp') }}" alt="Logo">
                        </div>
                        <div class="col-md-8 col-sm-7 col-6">
                            <ul class="main_nav">
                                <li><a href="{{ url('/') }}">{{ $text['menu']['home'] }}</a></li>
                                <li><a href="#contacts">{{ $text['menu']['contacts'] }}</a></li>
                            </ul>

                            <a class="float-right show_little_nav"><i class="fa fa-list"></i></a>
                        </div>

                        <div class="col-md-2 col-sm-2 col-4">
                            <ul class="languages">
                                <li class="@if($lang === 'ro') selected @endif"><a
                                        href="?lang=ro">RO</a></li>
                                <li class="@if($lang === 'ru') selected @endif"><a href="?lang=ru">RU</a></li>
                                <li class="@if($lang === 'en') selected @endif"><a href="?lang=en">EN</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row"></div>
                </div>

                <div class="bottom_part">
                    <button type="button" class="consult_btn"
                            data-toggle="modal" data-target="#formModal">{{ $text['form']['btn_consult'] }}</button>
                </div>
            </div>
        </section>

        {{--MOVEABLE NAVIGATION Second--}}
        <section class="little_nav cS-hidden">
            <div class="container">
                <div class="row top_contacts">
                    <ul class="contacts list-unstyled">
                        <li class="text-right">
                            <img src="{{ asset('images/icons/phone.webp') }}" alt="">
                            <span><a class="phone" href="tel:+37379998902">+373 79998902</a></span>
                        </li>
                        <li class="text-right">
                            <img src="{{ asset('images/icons/mail.webp') }}" alt="">
                            <span>info@avocat-moldova.md</span>
                        </li>
                    </ul>
                </div>

                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2">
                        <img class="main_logo" src="{{ asset('images/logo.webp') }}" alt="Logo">
                    </div>
                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-4 col-5">
                        <ul class="main_nav">
                            <li><a href="{{ url('/') }}">{{ $text['menu']['home'] }}</a></li>
                            <li><a href="#contacts">{{ $text['menu']['contacts'] }}</a></li>
                        </ul>

                        <a class="float-right show_little_nav"><i class="fa fa-list"></i></a>
                    </div>

                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-5 col-5">
                        <ul class="languages">
                            <li class="@if($lang === 'ro') selected @endif"><a href="?lang=ro">RO</a></li>
                            <li class="@if($lang === 'ru') selected @endif"><a href="?lang=ru">RU</a></li>
                            <li class="@if($lang === 'en') selected @endif"><a href="?lang=en">EN</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </section>

        @yield('content')

        <section class="find_us">
            <div class="grey_border"></div>
            <div class="header">
                <h1 class="title_clean">{{ $text['titles']['find_us'] }}</h1>
            </div>

            <div id="map"></div>


        </section>

        <section id="contacts" class="footer_contacts">
            <div class="grey_border"></div>
            <div class="header">
                <h1 class="title_clean text-center">{{ $text['titles']['contacts'] }}</h1>
            </div>

            <div class="container">
                <div class="form_contacts">
                    <h4>{{ $text['titles']['form_title'] }}</h4>
                    <h3>{{ $text['titles']['form_subtitle'] }}</h3>

                    <form class="form_apply" id="contact_form">
                        <div class="form-group">
                            <input type="text" name="name" autocomplete="false"
                                   placeholder="{{ $text['form']['name'] }}" class="form-control">
                            <input type="text" name="phone" autocomplete="false"
                                   placeholder="{{ $text['form']['phone'] }}" class="form-control">
                        </div>

                        <div class="form-group">
                                <textarea placeholder="{{ $text['form']['problem'] }}"
                                          name="problem" class="form-control" style="resize: none;" rows="3"></textarea>
                        </div>

                        <div class="form-group">
                            <button class="send_btn contact_btn"
                                    type="button">
                                <i class="fa fa-send"></i>
                                {{ $text['form']['btn_send'] }}</button>
                        </div>
                    </form>

                    <ul class="contacts_details">
                        <li><img src="{{ asset('images/contacts/place.png') }}" alt=""> {{
                            $text['contacts']['street'] }}</li>
                        <li><img src="{{ asset('images/contacts/phone.png') }}" alt=""> <a
                                href="tel:+37379998902">+373 79998902</a></li>
                        <li><img src="{{ asset('images/contacts/email.png') }}" alt=""> <a
                                href="">info@avocat-moldova.md</a></li>
                    </ul>

                    <ul class="list-unstyled bottom_social_links">
                        <li>
                            <a target="_blank"
                               href="http://www.facebook.com/pages/Cabinetul-avocatului-Veaceslav-Spalatu/786247954759235?timeline_context_item_type=intro_card_work&timeline_context_item_source=100001098787548&fref=tag">
                                <img src="{{ asset('images/icons/fb2.webp') }}" alt="">
                            </a>
                        </li>
                        <li><a target="_blank"
                               href="http://ok.ru/profile/575963072404">
                                <img src="{{ asset('images/icons/ok2.webp') }}" alt="">
                            </a></li>
                        <li><a target="_blank"
                               href="http://www.linkedin.com/in/avocat-moldova-88148116a/">
                                <img src="{{ asset('images/icons/link2.webp') }}" alt="">
                            </a></li>
                    </ul>
                </div>
            </div>

        </section>

        <footer>
            <div class="grey_border"></div>
            <div class="copywrite">Designed by <a style="color: #6CE8E0;" target="_blank"
                                                  href="http://artyweb.md">ArtyWeb</a>.
                All rights reserverd by Spalatu Veaceslav 2007 - {{ date('Y') }}.
            </div>
        </footer>

    </div>{{--End Container Fluid--}}

</div>{{--End Main Wrapper--}}

<script src="{{ asset('libs/jquery.js') }}"></script>
<script src="{{ asset('libs/jquery_cookie.js') }}"></script>
<script src="{{ asset('libs/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('libs/jquery-validate/jquery.validate.js') }}"></script>
<script src="{{ asset('libs/jquery-validate/additional-methods.js') }}"></script>
<script src="{{ asset('libs/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<script src="https://api-maps.yandex.ru/2.1/?apikey=e79c067a-27b2-4fbc-8118-559591e35858&lang={{ $yandex_lang }}"
        type="text/javascript"></script>
    <script>
		ymaps.ready(function () {
			var myMap = new ymaps.Map('map', {
					center: [46.9768042, 28.8531098],
					zoom: 16
				}, {
					searchControlProvider: 'yandex#search'
				}),

				// Создаём макет содержимого.
				MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
					'<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
				),

				myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
					hintContent: 'Собственный значок метки',
					balloonContent: 'Это красивая метка'
				}, {
					// Опции.
					// Необходимо указать данный тип макета.
					iconLayout: 'default#image',
					// Своё изображение иконки метки.
					iconImageHref: 'images/location.png',
					// Размеры метки.
					iconImageSize: [30, 50],
					// Смещение левого верхнего угла иконки относительно
					// её "ножки" (точки привязки).
					iconImageOffset: [-5, -38]
				});

			myMap.geoObjects
				.add(myPlacemark);
		});

    </script>

</body>
</html>