@extends('front.layouts.main')

@section('content')
    <section id="help_with" class="help_with">
        <img class="moveable_logo" src="{{ asset('images/blue_logo_white_bg_small.webp') }}" alt="">
        <img class="blue_logo" src="{{ asset('images/blue_logo.webp') }}" alt="">

        <div class="help_with_wrapper">
            <div class="container">
                <h1 class="text-center title">{{ $text['help_with']['title'] }}:</h1>

                <div class="row">
                    <div class="col-md-6">
                        <div class="photo_wrap float-right">
                            <div class="item" nr="1">
                                <a data-toggle="modal" data-target="#freeConsultModal">
                                    <img src="{{ asset('images/help_with/1.webp') }}" alt="">
                                </a>
                            </div>
                            <h5 class="text-center">1. {!! $text['help_with'][1] !!} </h5>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="photo_wrap float-left">
                            <div class="item" nr="2">
                                <a data-toggle="modal" data-target="#publicServiceModal">
                                    <img src="{{ asset('images/help_with/2.webp') }}" alt="">
                                </a>
                            </div>
                            <h5 class="text-center">2. {!! $text['help_with'][2] !!}</h5>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="photo_wrap float-right">
                            <div class="item" nr="3">
                                <a data-toggle="modal" data-target="#businessModal">
                                    <img src="{{ asset('images/help_with/3.webp') }}" alt="">
                                </a>
                            </div>
                            <h5 class="text-center">3. {!! $text['help_with'][3] !!}</h5>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item" nr="4">
                            <a data-toggle="modal" data-target="#businessAssistenceModal">
                                <img src="{{ asset('images/help_with/4.webp') }}" alt="">
                            </a>
                        </div>
                        <h5 class="text-center">4. {!! $text['help_with'][4] !!}</h5>
                    </div>
                    <div class="col-md-4">
                        <div class="photo_wrap float-left">
                            <div class="item" nr="5">
                                <a data-toggle="modal" data-target="#imobilesModal">
                                    <img src="{{ asset('images/help_with/5.webp') }}" alt="">
                                </a>
                            </div>
                            <h5 class="text-center">5. {!! $text['help_with'][5] !!}</h5>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="item" nr="6">
                            <a data-toggle="modal" data-target="#debtsModal">
                                <img src="{{ asset('images/help_with/6.webp') }}" alt="">
                            </a>
                        </div>
                        <h5 class="text-center">6. {!! $text['help_with'][6] !!}</h5>
                    </div>
                    <div class="col-md-3">
                        <div class="item" nr="7">
                            <a data-toggle="modal" data-target="#aviaModal">
                                <img src="{{ asset('images/help_with/7.webp') }}" alt="">
                            </a>
                        </div>
                        <h5 class="text-center">7. {!! $text['help_with'][7] !!}</h5>
                    </div>
                    <div class="col-md-3">
                        <div class="item" nr="8">
                            <a data-toggle="modal" data-target="#transportModal">
                                <img src="{{ asset('images/help_with/8.webp') }}" alt="">
                            </a>
                        </div>
                        <h5 class="text-center">8. {!! $text['help_with'][8] !!}</h5>
                    </div>
                    <div class="col-md-3">
                        <div class="item" nr="9">
                            <a data-toggle="modal" data-target="#emigrantsModal">
                                <img src="{{ asset('images/help_with/9.webp') }}" alt="">
                            </a>

                        </div>
                        <h5 class="text-center">9. {!! $text['help_with'][9] !!}</h5>
                    </div>
                </div>

                <div class="row">
                    <button type="button" class="consult_btn_blue"
                            data-toggle="modal" data-target="#formModal">{{ $text['form']['btn_consult']
                                    }}</button>
                </div>

            </div>
        </div>
    </section>

    <section class="work_toghether">
        <div class="container">
            <div class="titles_wrap">
                <h1>{{ $text['deviza'][0] }}</h1>
                <h1>{{ $text['deviza'][1] }}</h1>
            </div>
        </div>
    </section>

    @include('front.modals.principles.profesionalizm')
    @include('front.modals.principles.corectitudine')
    @include('front.modals.principles.incredere')
    @include('front.modals.principles.punctualitate')
    @include('front.modals.principles.responsabilitate')
    @include('front.modals.principles.comunicabilitate')
    <section id="about_us" class="about_us">
        <div class="grey_border"></div>
        <div class="header">
            <h1 class="title_white">{{ $text['about_us']['title'] }}</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-2 col-md-0">
                    <img class="left_side_img"
                         src="{{ asset('images/jurist.webp') }}" alt="Veaceslav Spalatu">
                </div>
                <div class="col-xl-8 col-lg-10 col-md-12">
                    <br>
                    <h4 class="text-center">Veaceslav Spalatu</h4>
                    <h4 class="second_jurist_title text-center">{{ $text['about_us']['subtitle'] }}</h4>

                    <img class="jurist_img_on_mobile"
                         src="{{ asset('images/jurist.webp') }}" alt="Veaceslav Spalatu">

                    <p class="intro">{{ $text['about_us']['description'] }}</p>

                    <div class="principles">
                        <h4>{{ $text['about_us']['principles']['title'] }}</h4>
                        <div data-toggle="modal" data-target="#profModal">
                            <h6>{{ $text['about_us']['principles']['professionalism'] }}</h6>
                            <img src="{{ asset('images/about_us/smart.webp') }}" alt=""></div>
                        <div data-toggle="modal" data-target="#corectModal">
                            <h6>{{ $text['about_us']['principles']['correct'] }}</h6>
                            <img src="{{ asset('images/about_us/corect.webp') }}" alt=""></div>
                        <div data-toggle="modal" data-target="#trustModal">
                            <h6>{{ $text['about_us']['principles']['trust'] }}</h6>
                            <img src="{{ asset('images/about_us/hands2.webp') }}"/></div>
                        <div data-toggle="modal" data-target="#punctualModal">
                            <h6>{{ $text['about_us']['principles']['punctuality'] }}</h6>
                            <img src="{{ asset('images/about_us/watch.webp') }}" alt=""></div>
                        <div data-toggle="modal" data-target="#responsabilModal">
                            <h6>{{ $text['about_us']['principles']['responsability'] }}</h6>
                            <img src="{{ asset('images/about_us/man.webp') }}" alt=""></div>
                        <div data-toggle="modal" data-target="#comunicabilModal">
                            <h6>{{ $text['about_us']['principles']['comunicability'] }}</h6>
                            <img src="{{ asset('images/about_us/speak.webp') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="button_space">
                <button type="button" class="consult_btn_blue"
                        data-toggle="modal" data-target="#formModal">{{ $text['form']['btn_consult'] }}</button>
            </div>
        </div>
    </section>

    <section id="domains" class="domains">
        <div class="domains_wrap">
            <h1 class="title">{{ $text['domains']['title'] }}</h1>

            <div class="container">
                <div class="row">
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#business_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/1.webp') }}">
                            </a>
                        </div>
                        <h5>{!! $text['domains'][0] !!}</h5>
                    </div>
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#civil_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/2.webp') }}">
                            </a>
                        </div>
                        <h5>{!! $text['domains'][1] !!}</h5>
                    </div>
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#family_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/3.webp') }}">
                            </a>

                        </div>
                        <h5>{!! $text['domains'][2] !!}</h5>
                    </div>
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#work_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/4.webp') }}">
                            </a>

                        </div>
                        <h5>{!! $text['domains'][3] !!}</h5>
                    </div>
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#intelectual_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/5.webp')
                                        }}">
                            </a>
                        </div>
                        <h5>{!! $text['domains'][4] !!}</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#migrational_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/6.webp')}}">
                            </a>
                        </div>
                        <h5>{!! $text['domains'][5] !!}</h5>
                    </div>
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#administrativ_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/7.webp') }}">
                            </a>
                        </div>
                        <h5>{!! $text['domains'][6] !!}</h5>
                    </div>
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#penal_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/8.webp')
                                        }}">
                            </a>
                        </div>
                        <h5>{!! $text['domains'][7] !!}</h5>
                    </div>
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#execution_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/9.webp')
                                        }}">
                            </a>
                        </div>
                        <h5>{!! $text['domains'][8] !!}</h5>
                    </div>
                    <div class="width_20">
                        <div class="item">
                            <a data-toggle="modal" data-target="#cedo_rightModal">
                                <img class="embed-responsive" src="{{ asset('images/acitvity_domains/10.webp')
                                        }}">
                            </a>
                        </div>
                        <h5>{!! $text['domains'][9] !!}</h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="button_space mb-3">
            <button type="button" class="consult_btn_blue"
                    data-toggle="modal" data-target="#formModal">{{ $text['form']['btn_consult'] }}</button>
        </div>
    </section>

    <section class="partners">
        <div class="grey_border"></div>
        <div class="header">
            <h1 class="title_clean">{{ $text['titles']['partners'] }}</h1>
        </div>
        <div class="wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 part_bisar">
                        <img class="embed-responsive"
                             src="{{ asset('images/partners/bisar.webp') }}" alt="">
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                        <img class="embed-responsive"
                             src="{{ asset('images/partners/electro.webp') }}" alt="">
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                        <img class="embed-responsive"
                             src="{{ asset('images/partners/leovicta.webp') }}" alt="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                        <img class="embed-responsive sirap_img"
                             src="{{ asset('images/partners/sirap.webp') }}" alt="">
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                        <img class="embed-responsive"
                             src="{{ asset('images/partners/expressdesign.webp')}}" alt="">
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 part_dac">
                        <img class="embed-responsive"
                             src="{{ asset('images/partners/dac.webp') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if($posts)
        <section id="blog_with" class="blog_with">
            <div class="blog_with_wrapper p-8">
                <div class="container">
                    <h1 class="text-center title">{{ $text['help_with']['title2'] }}:</h1>
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-lg-4 col-8 portfolio-item">
                                <div class="card h-100">
                                    <a target="_blank" href="{{ url('/post/')}}/{{ $post['slug'] }}">
                                        @if (isset($post['image']))
                                            <img class="card-img-top" src="{{ asset("images/posts/{$post['image']}") }}">
                                        @else
                                            <img class="card-img-top" src="{{ asset("images/posts/default.png") }}">
                                        @endif
                                    </a>
                                    <div class="card-body">
                                        <h2 class="card-title text-uppercase">
                                            <a target="_blank" href="{{ url('/post/')}}/{{ $post['slug'] }}">
                                                {{ $post["title_$lang"] }}
                                            </a>
                                        </h2>
                                        <p class="card-text">
                                            {!! substr($post["body_$lang"], 0, 180) . ' ...' !!}
                                        </p>
                                        <a target="_blank" href="{{ url('/post/')}}/{{ $post['slug'] }}"
                                           class="more_btn_blue">{{ $text['blog']['more'] }}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <a target="_blank" href="{{ url('/posts') }}"
                           class="consult_btn_blue">{{ $text['blog']['articles'] }}</a>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection