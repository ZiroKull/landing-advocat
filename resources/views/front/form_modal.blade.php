{{--
<!-- Modal -->
<div class="modal fade formModal" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('images/logo.png') }}" alt="">
                <h4 class="modal-title text-center title_white" id="formModalLabel">{{ $text['form']['title'] }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
						<div id="exTab3" class="container">
							<ul  class="nav flex-column nav-pills">
								<li class="active">
									<a  href="#1b" data-toggle="tab">{{ $text['tabs']['tab1'] }}</a>
								</li>
								<li>
									<a href="#2b" data-toggle="tab">{{ $text['tabs']['tab2'] }}</a>
								</li>
								<li>
									<a href="#3b" data-toggle="tab">{{ $text['tabs']['tab3'] }}</a>
								</li>
								<li>
									<a href="#4a" data-toggle="tab">{{ $text['tabs']['tab4'] }}</a>
								</li>
							</ul>

							<div class="tab-content clearfix">
								<div class="tab-pane active" id="1b">
									<h4>{{ $text['form']['title_pay'] }}</h4>
										<!--<div class="form-group">
											<input type="text" name="name" autocomplete="false"
												   placeholder="{{ $text['form']['name'] }}" class="form-control">
											<input type="text" name="phone" autocomplete="false"
												   placeholder="{{ $text['form']['phone'] }}" class="form-control">
										</div>
										<div class="form-group">
											<textarea style="width: 90%; margin: 0 auto; resize: none;"
													placeholder="{{ $text['form']['problem'] }}"
													  name="problem" class="form-control" rows="3"></textarea>
										</div>-->

                    <form name="payment"
                       method="post" action="https://sci.interkassa.com/" accept-charset="UTF-8">
                       <input type="hidden" name="ik_co_id" value="5c175be63b1eaf68308b456a"/>
                       <input type="hidden" name="ik_pm_no" value=""/>
                       <input type="hidden" name="ik_am" value="{{ $settings->rub }}"/>
                 <input type="hidden" name="ik_cur" value="rub"/>
					   <input type="hidden" name="ik_x_field1" value="{{ $lang }}"/>
                       <input type="hidden" name="ik_desc" value="Consultation"/>
                       <div class="form-group">
   											<button class="btn btn-primary send_btn" type="submit">
   												<i class="fa fa-send"></i>
   												{{ $text['form']['btn_pay'] }}</button>
   										</div>
                   </form>

								</div>
								<div class="tab-pane" id="2b">
									<center>
										<img src="{{ asset('images/carduri_bancare.svg') }}" width="150px" height="150px">
									</center>
									<h4>{{ $text['tabs']['desc2'] }}</h4>
									<div>
										<p>{{ $text['tabs']['mcart_bank'] }}:</p>
										<p>{{ $text['tabs']['mcart_title'] }}</p>
										<p>{{ $text['tabs']['mcart_numb'] }}</p>
										<p>{{ $text['tabs']['mcart_acc'] }}</p>
									</div>
								</div>
								<div class="tab-pane" id="3b">
									<center>
										<img src="{{ asset('images/terminale.svg') }}" width="150px" height="150px">
									</center>
									<h4>{{ $text['tabs']['desc3'] }}</h4>
									<div>
										<p>{{ $text['tabs']['qiwi'] }}</p>
									</div>
								</div>
								<div class="tab-pane" id="4a">
									<center>
										<img src="{{ asset('images/pay_by_cash.svg') }}" width="150px" height="150px">
									</center>
									<h4>{{ $text['tabs']['desc4'] }}</h4>
									<div>
										<p>{{ $text['tabs']['cabinet'] }}</p>
										<p>{{ $text['tabs']['adress'] }}</p>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
--}}

{{--OLD MODAL--}}

<!-- Modal -->
<div class="modal fade formModal" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('images/logo.webp') }}" alt="Logo">
                <h4 class="modal-title text-center title_white" id="formModalLabel">{{ $text['form']['title'] }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form autocomplete="off" id="modal_form" class="form_apply">
                            <div class="form-group">
                                <input type="text" name="name" autocomplete="false"
                                       placeholder="{{ $text['form']['name'] }}" class="form-control">
                                <input type="text" name="phone" autocomplete="false"
                                       placeholder="{{ $text['form']['phone'] }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <textarea style="width: 90%; margin: 0 auto; resize: none;"
                                          placeholder="{{ $text['form']['problem'] }}"
                                          name="problem" class="form-control" rows="3"></textarea>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input terms" name="terms">
                                <label class="form-check-label" for="terms">{{ $text['form']['terms1'] }}
                                    <a href="{{ url('/terms/')}}" target="_blank">{{ $text['form']['terms2'] }}</a>
                                </label>
                            </div>
                            <div class="form-group mt-5">
                                <button class="send_btn" type="submit">
                                    <i class="fa fa-send"></i>
                                    {{ $text['form']['btn_send'] }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>