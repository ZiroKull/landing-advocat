 <footer class="main-footer">
    <div class="pull-right hidden-xs"></div>
    <strong>Copyright &copy; {{ Carbon\carbon::now()->year }} .</strong> All rights reserved.
    <script src="{{ asset('admin/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('admin/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{ asset('admin/plugins/morris/morris.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('admin/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('admin/plugins/knob/jquery.knob.js') }}"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('admin/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ asset('admin/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('admin/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('admin/plugins/fastclick/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('admin/dist/js/app.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    {{-- <script src="{{ asset('admin/dist/js/pages/dashboard.js') }}"></script> --}}
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('admin/dist/js/demo.js') }}"></script>

     <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

     @if (request()->is('admin/customers'))
         <script src="{{ asset('admin/responsivetables/js/stacktable.js') }}"></script>

         <script type="text/javascript">
             $(document).ready(function() {
                 $('#table').stacktable();
             } );
         </script>
     @endif
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
	height : "350",
	setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    },
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;

      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 1.5,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>

	<script type="text/javascript">
{{-- ajax Form Add Post--}}

	$(document).ready(function() {

		$(document).on('click','.create-modal', function() {
			$('#create').modal('show');
			$('.form-horizontal').show();
			$('.modal-title').text('Add Post');
		});

    $('.modal-footer').on('click', '.add', function() {

  		tinyMCE.triggerSave();

  		var data = {
  			'slug': $('#create input[name=slug]').val(),
  			'title_ro': $('#create input[name=title_ro]').val(),
  			'title_ru': $('#create input[name=title_ru]').val(),
  			'title_en': $('#create input[name=title_en]').val(),
  			'body_ro': $('#create textarea[name=body_ro]').val(),
  			'body_ru': $('#create textarea[name=body_ru]').val(),
  			'body_en': $('#create textarea[name=body_en]').val()
  		};

        tinyMCE.triggerSave();

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
				url:"{{ route('ajaxupload.action') }}",
				method:"POST",
				data:data,
				dataType:'JSON',
				error: function(err) {
					console.log(err);
				},
				success:function(data)
				{
					if ((data.errors)) {
						if (data.errors.slug) {
							$('.error.slug').removeClass('hidden');
							$('.error.slug').text(data.errors.slug);
						}

						if (data.errors.title_ro) {
							$('.error.title_ro').removeClass('hidden');
							$('.error.title_ro').text(data.errors.title_ro);
						}

						if (data.errors.title_ru) {
							$('.error.title_ru').removeClass('hidden');
							$('.error.title_ru').text(data.errors.title_ru);
						}

						if (data.errors.title_en) {
							$('.error.title_en').removeClass('hidden');
							$('.error.title_en').text(data.errors.title_en);
						}

						if (data.errors.body_ro) {
							$('.error.body_ro').removeClass('hidden');
							$('.error.body_ro').text(data.errors.body_ro);
						}

						if (data.errors.body_ru) {
							$('.error.body_ru').removeClass('hidden');
							$('.error.body_ru').text(data.errors.body_ru);
						}

						if (data.errors.body_en) {
							$('.error.body_en').removeClass('hidden');
							$('.error.body_en').text(data.errors.body_en);
						}
					} else {
						$('#create').modal('hide');
						$('#create input').val('');
						tinyMCE.get('body_ro').setContent('');
						tinyMCE.get('body_ru').setContent('');
						tinyMCE.get('body_en').setContent('');

						$('.error').remove();
						$('#table').append("<tr class='post" + data.id + "'>"+
						"<td>" + data.id + "</td>"+
						"<td class='post-thumb-" + data.id + "'> <div class='admin-wrap-img'><img src='{{ asset('images/logo.png') }}' width='200px'></div> </td>"+
						"<td>" + data.title_ro + "</td>"+
						"<td>" + data.title_ru + "</td>"+
						"<td>" + data.title_en + "</td>"+
						"<td> <button class='edit-image btn btn-warning btn-sm' data-id='" + data.id + "' ><i class='glyphicon glyphicon-picture'></i></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' ><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-title_ro='" + data.title_ro + "' data-body_ro='" + data.body_ro + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
						"</tr>");
					}
				}
			});

    });

    // function Edit POST
  	$(document).on('click', '.edit-modal', function() {

      var data = {
        '_token': $('input[name=_token]').val(),
        'id': $(this).data('id')
      };

      $.ajax({
        url:"{{ route('ajaxget.action') }}",
        method:"POST",
        data:data,
        dataType:'JSON',
        error: function(err) {
          console.log(err);
        },
        success:function(data)
        {
          console.log(data);

          $('#fid').val(data.id);
      		$('#edit input[name=slug]').val(data.slug);
      		$('#edit input[name=title_ro]').val( data.title_ro );
      		$('#edit input[name=title_ru]').val( data.title_ru );
      		$('#edit input[name=title_en]').val( data.title_en );

      		$('#edit textarea[name=body_ro]').val( data.body_ro );
      		$('#edit textarea[name=body_ru]').val( data.body_ru );
      		$('#edit textarea[name=body_en]').val( data.body_en );

      		tinyMCE.get('body_ro_2').setContent( data.body_ro );
      		tinyMCE.get('body_ru_2').setContent( data.body_ru );
      		tinyMCE.get('body_en_2').setContent( data.body_en );

          $('#edit').modal('show');
        }
      });
  	});

    $('.modal-footer').on('click', '.edit', function() {

  		tinyMCE.triggerSave();

  		var data = {
  			'_token': $('input[name=_token]').val(),
  			'id': $("#fid").val(),
  			'slug': $('#edit input[name=slug]').val(),
  			'title_ro': $('#edit input[name=title_ro]').val(),
  			'title_ru': $('#edit input[name=title_ru]').val(),
  			'title_en': $('#edit input[name=title_en]').val(),
  			'body_ro': $('#edit textarea[name=body_ro]').val(),
  			'body_ru': $('#edit textarea[name=body_ru]').val(),
  			'body_en': $('#edit textarea[name=body_en]').val()
  		};

  		console.log(data);

  		$.ajax({
  		type: 'POST',
  		url: "{{ route('ajaxupdate.action') }}",
  		data:  data,
  		error: function(err) {
  			console.log();
  		},
  		success: function(data) {

  			if ((data.errors)) {
  				if (data.errors.slug) {
  					$('#edit .error.slug').removeClass('hidden');
  					$('#edit .error.slug').text(data.errors.slug);
  				}

  				if (data.errors.title_ro) {
  					$('#edit .error.title_ro').removeClass('hidden');
  					$('#edit .error.title_ro').text(data.errors.title_ro);
  				}

  				if (data.errors.title_ru) {
  					$('#edit .error.title_ru').removeClass('hidden');
  					$('#edit .error.title_ru').text(data.errors.title_ru);
  				}

  				if (data.errors.title_en) {
  					$('#edit .error.title_en').removeClass('hidden');
  					$('#edit .error.title_en').text(data.errors.title_en);
  				}

  				if (data.errors.body_ro) {
  					$('#edit .error.body_ro').removeClass('hidden');
  					$('#edit .error.body_ro').text(data.errors.body_ro);
  				}

  				if (data.errors.body_ru) {
  					$('#edit .error.body_ru').removeClass('hidden');
  					$('#edit .error.body_ru').text(data.errors.body_ru);
  				}

  				if (data.errors.body_en) {
  					$('#edit .error.body_en').removeClass('hidden');
  					$('#edit .error.body_en').text(data.errors.body_en);
  				}
  			} else {
          var img = '';
          if (data.image) {
            img = "<td class='post-thumb-" + data.id + "'> <img src='{{ asset('images/posts/') }}/" + data.image + "' width='200px'> </td>";
          } else {
            img = "<td class='post-thumb-" + data.id + "'> <div class='admin-wrap-img'><img src='{{ asset('images/posts/default.png') }}' width='200px'></div> </td>";
          }

  				$('#edit').modal('hide');
  				$('.post' + data.id).replaceWith(" "+
  				  "<tr class='post" + data.id + "'>"+
  				  "<td>" + data.id + "</td>"+ img +
  				  "<td>" + data.title_ro + "</td>"+
  				  "<td>" + data.title_ru + "</td>"+
  				  "<td>" + data.title_en + "</td>"+
  				  "<td> <button class='edit-image btn btn-warning btn-sm' data-id='" + data.id + "' ><i class='glyphicon glyphicon-picture'></i></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "'  ><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' ><span class='glyphicon glyphicon-trash'></span></button></td>"+
  				  "</tr>");
  			}


  		}
  	  });
  	});

    // form Delete function
  	$(document).on('click', '.delete-modal', function() {
  		$('#footer_action_button_del').text(" Delete");
  		$('#footer_action_button_del').removeClass('glyphicon-check');
  		$('#footer_action_button_del').addClass('glyphicon-trash');
  		$('.actionBtn').removeClass('btn-success');
  		$('.actionBtn').addClass('btn-danger');
  		$('.actionBtn').addClass('delete');
  		$('.modal-title').text('Delete Post');
  		$('.id').text($(this).data('id'));
  		$('.deleteContent').show();
  		$('.form-horizontal').hide();
  		$('.title').html($(this).data('title'));
  		$('#delete').modal('show');
  	});

  	$('.modal-footer').on('click', '.delete', function(){

  		var data = {
  			  '_token': $('input[name=_token]').val(),
  			  'id': $('.id').text()
  			};

  		console.log( data );

  		$.ajax({
  			type: 'POST',
  			url: '{{ url("/admin/") }}/deletePost',
  			data: data,
  			success: function(data){
  			   $('.post' + $('.id').text()).remove();
  			   $('#delete').modal('hide');
  			}
  		});
  	});

    $(document).on('click', '.edit-image', function() {
  		$('#imgid').val($(this).data('id'));
  		$('#addImage').modal('show');
  	});
    //add-image

    $('#image_form').on('submit', function(event){
      event.preventDefault();

      console.log('image_form');

      $.ajax({
       url:"{{ route('ajaximage.action') }}",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       error: function(err) {
         console.log(err);
         $('#message').css('display', 'block');
         $('#message').html('Image Upload Error');
         $('#message').addClass('alert-info');
       },
       success:function(data)
       {
         console.log(data);
         $('#message').css('display', 'block');
         $('#message').html(data.message);
         $('#message').addClass(data.class_name);
         $('.post-thumb-' + data.id).html(data.uploaded_image);

         setTimeout(function() {
            $('#addImage').modal('hide');
          }, 1000);
       }
      })
     });





  	// form Settings function
  	$(document).on('click', '.settings-modal', function() {

  		$('.sid').text($(this).data('id'));
  		$('#settings input[name=price_lei]').val($(this).data('lei'));
  		$('#settings input[name=price_rub]').val($(this).data('rub'));
  		$('#settings').modal('show');
  	});

  	$('.modal-footer').on('click', '.settings', function(){

  		var data = {
  			  '_token': $('input[name=_token]').val(),
  			  'id': $('.sid').text(),
  			  'lei': $('#settings input[name=price_lei]').val(),
  			  'rub': $('#settings input[name=price_rub]').val(),
  			  'desc': 'Price for consultation'
  			};

  		console.log( data );

  		$.ajax({
  			type: 'POST',
  			url: '{{ url("/admin/") }}/editSettings',
  			data: data,
  			success: function(data){

  			   if ((data.errors)) {
  					if (data.errors.lei) {
  						alert(data.errors.lei);
  					}

  					if (data.errors.rub) {
  						alert(data.errors.rub);
  					}
  				} else {
  					$('#settings').modal('hide');
  					$('.settings' + data.id).replaceWith(" "+
  					  "<tr class='settings" + data.id + "'>"+
  					  "<td>1</td>"+
  					  "<td>" + data.lei + "</td>"+
  					  "<td>" + data.rub + "</td>"+
  					  "<td>" + data.desc + "</td>"+
  					  "<td class=\"text-center\">"+
  							"<a href=\"#\" class=\"settings-modal btn btn-warning btn-sm\" data-id='"+ data.id +"' data-lei='"+ data.lei +"' data-usd='"+ data.usd +"' >"+
  							  "<i class=\"glyphicon glyphicon-pencil\"></i>"+
  							"</a>"+
  					  "</tr>");
  				}
  			}
  		});
  	});


	});

</script>

    @section('footerSection')
    @show
  </footer>
