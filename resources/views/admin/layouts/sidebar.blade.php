<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('images/logo.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ (request()->is('admin/home')) ? 'active' : '' }} treeview">
                <a href="{{ route('admin.home') }}"><i class="fa fa-circle-o"></i> Posts</a>
            </li>
            <li class="{{ (request()->is('admin/settings')) ? 'active' : '' }} treeview">
                <a href="{{ route('settings.index') }}"><i class="fa fa-circle-o"></i> Settings</a>
            </li>
            <li class="{{ (request()->is('admin/customers')) ? 'active' : '' }} treeview">
                <a href="{{ route('customers.index') }}"><i class="fa fa-circle-o"></i> Customers</a>
            </li>
        </ul>

    </section>
    <!-- /.sidebar -->
</aside>