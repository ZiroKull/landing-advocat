@extends('admin.layouts.app')

@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Admin Panel
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Posts</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box admin_posts">
                <div class="box-header with-border">
                    <h3 class="box-title">Posts</h3>

                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <div class="">
                        <div class="table table-responsive">
                            <table class="table table-bordered" id="table">
                                <tr>
                                    <th width="30px">No</th>
                                    <th width="270px">Thumbnail</th>
                                    <th>Title Ro</th>
                                    <th>Title Ru</th>
                                    <th>Title En</th>
                                    <th class="text-center" width="130px">
                                        <a href="#" class="create-modal btn btn-success btn-sm">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </a>
                                    </th>
                                </tr>
                                {{ csrf_field() }}
                                <?php  $no = 1; ?>
                                @foreach ($posts as $value)
                                    <tr class="post{{$value->id}}">
                                        <td>{{ $no++ }}</td>
                                        <td class="post-thumb-{{$value->id}}">
                                            @if (isset($value->image))
                                                <img src="{{ url('/') }}/images/posts/{{ $value->image }}" alt="Image">
                                            @else
                                                <img src="{{ url('/') }}/images/posts/default.png" alt="Image">
                                            @endif
                                        </td>
                                        <td>{{ $value->title_ro }}</td>
                                        <td>{{ $value->title_ru }}</td>
                                        <td>{{ $value->title_en }}</td>
                                        <td>
                                            <button class="edit-image btn btn-warning btn-sm" data-id="{{$value->id}}">
                                                <i class="glyphicon glyphicon-picture"></i>
                                            </button>
                                            <button class="edit-modal btn btn-warning btn-sm" data-id="{{$value->id}}">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>
                                            <button class="delete-modal btn btn-danger btn-sm" data-id="{{$value->id}}">
                                                <i class="glyphicon glyphicon-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        {{$posts->links()}}
                    </div>
                    {{-- Modal Form Create Post --}}
                    <div id="create" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <form action="{{ route('ajaxupload.action') }}" method="post" id="upload_form"
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="form-group row add">
                                            <label class="control-label col-sm-2" for="body">Slug :</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="slug"
                                                       placeholder="Post url Here" required>
                                                <p class="error slug text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                        <div class="form-group row add">
                                            <label class="control-label col-sm-2" for="title">Title Ro :</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="title_ro"
                                                       placeholder="Your Title Ro Here" required>
                                                <p class="error title_ro text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                        <div class="form-group row add">
                                            <label class="control-label col-sm-2" for="title">Title Ru :</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="title_ru"
                                                       placeholder="Your Title Ru Here" required>
                                                <p class="error title_ru text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                        <div class="form-group row add">
                                            <label class="control-label col-sm-2" for="title">Title En :</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="title_en"
                                                       placeholder="Your Title En Here" required>
                                                <p class="error title_en text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>

                                        <div class="form-group row add">
                                            <label class="control-label col-sm-2" for="body">Body Ro :</label>
                                            <div class="col-sm-10">
						  <textarea class="form-control my-editor" id="body_ro" name="body_ro"
                                    placeholder="Your Body Ro Here" required></textarea>
                                                <p class="error body_ro text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                        <div class="form-group row add">
                                            <label class="control-label col-sm-2" for="body">Body Ru :</label>
                                            <div class="col-sm-10">
						  <textarea class="form-control my-editor" id="body_ru" name="body_ru"
                                    placeholder="Your Body Ru Here" required></textarea>
                                                <p class="error body_ru text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                        <div class="form-group row add">
                                            <label class="control-label col-sm-2" for="body">Body En :</label>
                                            <div class="col-sm-10">
						  <textarea class="form-control my-editor" id="body_en" name="body_en"
                                    placeholder="Your Body En Here" required></textarea>
                                                <p class="error body_en text-center alert alert-danger hidden"></p>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-warning add" type="submit" name="upload" id="upload">
                                        <span class="glyphicon glyphicon-plus"></span>Save Post
                                    </button>

                                    <button class="btn btn-warning" type="button" data-dismiss="modal">
                                        <span class="glyphicon glyphicon-remobe"></span>Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Modal Form Edit --}}
                <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('ajaxupdate.action') }}" method="post" id="update_form"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" class="form-control" id="fid" disabled>

                                    <div class="form-group row add">
                                        <label class="control-label col-sm-2" for="body">Slug :</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="slug"
                                                   placeholder="Post url Here" value="" required>
                                            <p class="error slug text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row add">
                                        <label class="control-label col-sm-2" for="title">Title Ro :</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="title_ro"
                                                   placeholder="Your Title Ro Here" value="" required>
                                            <p class="error title_ro text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row add">
                                        <label class="control-label col-sm-2" for="title">Title Ru :</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="title_ru"
                                                   placeholder="Your Title Ru Here" value="" required>
                                            <p class="error title_ru text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row add">
                                        <label class="control-label col-sm-2" for="title">Title En :</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" value="" name="title_en"
                                                   placeholder="Your Title En Here" required>
                                            <p class="error title_en text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row add">
                                        <label class="control-label col-sm-2" for="body">Body Ro :</label>
                                        <div class="col-sm-10">
						  <textarea class="form-control my-editor body_ro" id="body_ro_2" name="body_ro"
                                    placeholder="Your Body Ro Here" required></textarea>
                                            <p class="error body_ro text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row add">
                                        <label class="control-label col-sm-2" for="body">Body Ru :</label>
                                        <div class="col-sm-10">
						  <textarea class="form-control my-editor body_ru" id="body_ru_2" name="body_ru"
                                    placeholder="Your Body Ru Here" required></textarea>
                                            <p class="error body_ru text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row add">
                                        <label class="control-label col-sm-2" for="body">Body En :</label>
                                        <div class="col-sm-10">
						  <textarea class="form-control my-editor body_en" id="body_en_2" name="body_en"
                                    placeholder="Your Body En Here" required></textarea>
                                            <p class="error body_en text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>


                                </form>

                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-warning edit" type="submit" name="upload" id="upload">
                                    <span class="glyphicon glyphicon-plus"></span>Update Post
                                </button>

                                <button class="btn btn-warning" type="button" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remobe"></span>Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Modal Form Delete --}}
                <div id="delete" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">

                                {{-- Form Delete Post --}}
                                <div class="deleteContent">
                                    Are You sure want to delete <span class="title"></span>?
                                    <span class="hidden id"></span>
                                </div>

                            </div>
                            <div class="modal-footer">

                                <button type="button" class="btn actionBtn">
                                    <span id="footer_action_button_del" class="glyphicon"></span>
                                </button>

                                <button type="button" class="btn btn-warning" data-dismiss="modal">
                                    <span class="glyphicon glyphicon"></span>close
                                </button>

                            </div>
                        </div>
                    </div>
                </div>

                {{-- Modal Form attachment --}}
                <div id="addImage" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Post Image</h4>
                            </div>
                            <div class="modal-body">

                                <div class="alert" id="message" style="display: none"></div>

                                <form action="{{ route('ajaximage.action') }}" method="post" id="image_form"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" class="form-control" id="imgid" name="id"/>

                                    <div class="form-group">
                                        <table class="table">
                                            <tr>
                                                <td width="40%" align="right"><label>Select File for Upload</label></td>
                                                <td width="60%"><input type="file" name="select_file" id="select_file"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="40%" align="right"></td>
                                                <td width="60%"><span class="text-muted">jpg, png, gif</span></td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="modal-footer">

                                        <button class="btn btn-warning add-image" type="submit" name="upload">
                                            <span id="footer_action_button_att"
                                                  class="glyphicon glyphicon-picture"></span>Upload
                                            Image
                                        </button>

                                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                                            <span class="glyphicon glyphicon"></span>close
                                        </button>

                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
