@extends('admin.layouts.app')

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Admin Panel
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Customers</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Customers</h3>

        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <div class="">
		  <div class="table table-responsive">
			<table class="table table-bordered" id="table">
			  <tr>
				<th width="90px">No</th>
				<th>Name</th>
				<th>Phone</th>
				<th>Problem</th>
				<th>Data</th>
				<th>Status</th>
				<th>Invoice Id</th>  
				<th>Payment №</th>  
			  </tr>

			  {{ csrf_field() }}
			  <?php  $no=1; ?>
			  @foreach ($customer as $value)
				<tr class="customer{{$value->id}}">
				  <td>{{ $no++ }}</td>
				  <td>{{ $value->name }}</td>
				  <td>{{ $value->phone }}</td>
				  <td>{{ $value->problem }}</td>
				  <td>{{ $value->time }}</td>
				  <td>{{ $value->status }}</td>
				  <td>{{ $value->ik_inv_id }}</td>
				  <td>{{ $value->ik_pm_no }}</td>	
				</tr>
			  @endforeach	
			</table>
		  </div>
		  {{$customer->links()}}	
		</div>
		</div>
	
      <!-- /.box-body -->
      <div class="box-footer"></div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection