@extends('admin.layouts.app')

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Admin Panel
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Settings</h3>

        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <div class="">
		  <div class="table table-responsive">
			<table class="table table-bordered" id="table">
			  <tr>
				<th width="90px">No</th>
				<th>Price Lei</th>
				<th>Price Rub</th>
				<th>Description</th>
				<th class="text-center" width="100px">Actions</th>
			  </tr>
			  {{ csrf_field() }}
			  <?php  $no=1; ?>
			  @foreach ($settings as $value)
				<tr class="settings{{$value->id}}">
				  <td>{{ $no++ }}</td>
				  <td>{{ $value->lei }}</td>
				  <td>{{ $value->rub }}</td>
				  <td>{{ $value->desc }}</td>
				  <td class="text-center">
					<a href="#" class="settings-modal btn btn-warning btn-sm" data-id="{{$value->id}}" data-lei="{{$value->lei}}" data-rub="{{$value->rub}}" >
					  <i class="glyphicon glyphicon-pencil"></i>
					</a>
				  </td>
				</tr>
			  @endforeach
			</table>
		  </div>

		</div>
		</div>
		{{-- Modal Form Settings --}}
		<div id="settings" class="modal fade" role="dialog">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Update Settings</h4>
			  </div>
			  <div class="modal-body">
				<form class="form-horizontal" role="form">
				  <div class="form-group row add">
					<label class="control-label col-sm-2" for="title">Price Lei :</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" id="price_lei" name="price_lei"
					  placeholder="Your Price Lei Here" required>
					  <p class="error text-center alert alert-danger hidden"></p>
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-sm-2" for="body">Price Rub :</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" id="price_rub" name="price_rub"
					  placeholder="Your Price Usd Here" required>
					  <p class="error text-center alert alert-danger hidden"></p>
					</div>
				  </div>
				</form>
				<span class="hidden sid"></span>
			  </div>
				  <div class="modal-footer">
					<button class="btn btn-warning settings"  >
					  <span class="glyphicon glyphicon-plus"></span>Update Settings
					</button>
					<button class="btn btn-warning" type="button" data-dismiss="modal">
					  <span class="glyphicon glyphicon-remobe"></span>Close
					</button>
				  </div>
			</div>
		  </div>
		</div>

      <!-- /.box-body -->
      <div class="box-footer"></div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
