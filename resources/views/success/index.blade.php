<!doctype html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W3Q2HS8');</script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77915149-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-77915149-2');
    </script>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Avocat Moldova | Blog</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/lightslider/css/lightslider.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media_query.css') }}">
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3Q2HS8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

@include('front.form_modal')

{{--LITTLE NAV FOR MOBILE--}}
<div class="little_nav_bar">
    <ul class="second_nav">
        <li><a href="{{ url('/') }}#main_wrapper">{{ $text['menu']['home'] }}</a></li>
        <li><a href="{{ url('/') }}#help_with">{{ $text['menu']['help'] }}</a></li>
        <li><a href="{{ url('/') }}#about_us">{{ $text['menu']['about_us'] }}</a></li>
        <li><a href="{{ url('/') }}#domains">{{ $text['menu']['domains'] }}</a></li>
        <li><a href="{{ url('/') }}#contacts">{{ $text['menu']['contacts'] }}</a></li>
		<li><a href="{{ url('/') }}blog">{{ $text['menu']['blog'] }}</a></li>
    </ul>
    <div class="close_little_btn"><i class="fa fa-angle-up"></i></div>
</div>

    <div id="main_wrapper">
        <div class="container-fluid">

            <section class="top_header">
                <div class="blue_line"></div>
                <div class="grey_border"></div>
            </section>

            {{--CLASSICAL NAVIGATION FOR PC--}}
            <section class="bottom_header" style="height: 380px;">
                <div class="blue_box"></div>
                <div class="container">
                    <div class="top_part">
                        <div class="row">
                            <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 offset-xl-7 offset-lg-7 offset-md-0">
                                <ul class="social_links list-unstyled">
                                    <li>
                                        <a      target="_blank"
                                                href="http://www.facebook.com/pages/Cabinetul-avocatului-Veaceslav-Spalatu/786247954759235?timeline_context_item_type=intro_card_work&timeline_context_item_source=100001098787548&fref=tag">
                                            <img src="{{ asset('images/icons/fb2.png') }}" alt="">
                                        </a>
                                    </li>
                                    <li><a  target="_blank"
                                            href="http://ok.ru/profile/575963072404">
                                            <img src="{{ asset('images/icons/ok2.png') }}" alt="">
                                        </a></li>
                                    <li><a      target="_blank"
                                                href="http://www.linkedin.com/in/avocat-moldova-88148116a/">
                                            <img src="{{ asset('images/icons/link2.png') }}" alt="">
                                        </a></li>
                                </ul>

                                <ul class="contacts list-unstyled">
                                    <li class="text-right">
                                        <img src="{{ asset('images/icons/phone.png') }}" alt="">
                                        <span><a class="phone" href="tel:+373 79998902">+373 79998902</a></span>
                                    </li>
                                    <li class="text-right">
                                        <img src="{{ asset('images/icons/mail.png') }}" alt="">
                                        <span>info@avocat-moldova.md</span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="row" id="second_menu_row">
                            <div class="col-md-2 col-sm-2 col-1">
                                <img class="main_logo" src="{{ asset('images/logo.png') }}" alt="Logo">
                            </div>
                            <div class="col-md-8 col-sm-7 col-6">
                                <ul class="main_nav">
                                    <li><a href="{{ request()->url() }}#main_wrapper">{{ $text['menu']['home'] }}</a></li>
                                    <li><a href="{{ url('/') }}#help_with">{{ $text['menu']['help'] }}</a></li>
                                    <li><a href="{{ url('/') }}#about_us">{{ $text['menu']['about_us'] }}</a></li>
                                    <li><a href="{{ url('/') }}#domains">{{ $text['menu']['domains'] }}</a></li>
                                    <li><a href="{{ url('/') }}#contacts">{{ $text['menu']['contacts'] }}</a></li>
									<li><a href="{{ url('/') }}blog">{{ $text['menu']['blog'] }}</a></li>
                                </ul>

                                <a class="float-right show_little_nav"><i class="fa fa-list"></i></a>
                            </div>

                            <div class="col-md-2 col-sm-2 col-4">
                                <ul class="languages">
                                    <li class="@if($lang === 'ro') selected @endif"><a
                                                href="?lang=ro">RO</a></li>
                                    <li class="@if($lang === 'ru') selected @endif"><a href="?lang=ru">RU</a></li>
                                    <li class="@if($lang === 'en') selected @endif"><a href="?lang=en">EN</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="row"></div>
                    </div>

                    <div class="bottom_part">
                        <button type="button" class="consult_btn"
                                data-toggle="modal" data-target="#formModal">{{ $text['form']['btn_consult'] }}</button>
                    </div>
                </div>
            </section>

            {{--MOVEABLE NAVIGATION Second--}}
            <section class="little_nav cS-hidden">
                <div class="container">
                    <div class="row top_contacts">
                        <ul class="contacts list-unstyled">
                            <li class="text-right">
                                <img src="{{ asset('images/icons/phone.png') }}" alt="">
                                <span><a class="phone" href="tel:+37379998902">+373 79998902</a></span>
                            </li>
                            <li class="text-right">
                                <img src="{{ asset('images/icons/mail.png') }}" alt="">
                                <span>info@avocat-moldova.md</span>
                            </li>
                        </ul>
                    </div>

                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2">
                            <img class="main_logo" src="{{ asset('images/logo.png') }}" alt="Logo">
                        </div>
                        <div class="col-lg-8 col-md-6 col-sm-6 col-xs-4 col-5">
                            <ul class="main_nav">
                                <li><a href="{{ url('/') }}#main_wrapper">{{ $text['menu']['home'] }}</a></li>
                                <li><a href="{{ url('/') }}#help_with">{{ $text['menu']['help'] }}</a></li>
                                <li><a href="{{ url('/') }}#about_us">{{ $text['menu']['about_us'] }}</a></li>
                                <li><a href="{{ url('/') }}#domains">{{ $text['menu']['domains'] }}</a></li>
                                <li><a href="{{ url('/') }}#contacts">{{ $text['menu']['contacts'] }}</a></li>
								<li><a href="{{ url('/') }}blog">{{ $text['menu']['blog'] }}</a></li>
                            </ul>

                            <a class="float-right show_little_nav"><i class="fa fa-list"></i></a>
                        </div>

                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-5 col-5">
                            <ul class="languages">
                                <li class="@if($lang === 'ro') selected @endif"><a href="?lang=ro">RO</a></li>
                                <li class="@if($lang === 'ru') selected @endif"><a href="?lang=ru">RU</a></li>
                                <li class="@if($lang === 'en') selected @endif"><a href="?lang=en">EN</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </section>

            <section id="blog_with" class="blog_with">
                <img class="moveable_logo" src="{{ asset('images/blue_logo_white_bg_small.png') }}" alt="">
                <img class="blue_logo" src="{{ asset('images/blue_logo.png') }}" alt="">

                <div class="blog_with_wrapper p-8">
                    <div class="container">
                        <h1 class="text-center title">{{ $text['help_with']['10'] }}:</h1>

						
						<div class="row">
							<div class="col-md-6 offset-3">
								<form autocomplete="off" id="modal_form"
									  class="form_apply_2">
									<div class="form-group">
										<input type="hidden" name="ik_inv_id" value="{{ $customer->ik_inv_id }}">
										<input type="text" name="name" autocomplete="false"
											   placeholder="{{ $text['form']['name'] }}" class="form-control">
										<input type="text" name="phone" autocomplete="false"
											   placeholder="{{ $text['form']['phone'] }}" class="form-control">
									</div>
									<div class="form-group">
										<textarea style="width: 90%; margin: 0 auto; resize: none;"
												placeholder="{{ $text['form']['problem'] }}"
												  name="problem" class="form-control" rows="3"></textarea>
									</div>
									<div class="form-group">
										<button class="send_btn_2" type="button">
											<i class="fa fa-send"></i>
											{{ $text['form']['btn_send'] }}</button>
									</div>
								</form>
							</div>
						</div>

                    </div>
                </div>
            </section>


            <footer>
                <div class="grey_border"></div>
                <div class="copywrite">Designed by RNB Colour. All rights reserverd by Spalatu Veaceslav 2007 - {{ date
                ('Y') }}.</div>
            </footer>

        </div>{{--End Container Fluid--}}

    </div>{{--End Main Wrapper--}}

    <script src="{{ asset('libs/jquery.js') }}"></script>
    <script src="{{ asset('libs/jquery_cookie.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('libs/jquery-validate/jquery.validate.js') }}"></script>
    <script src="{{ asset('libs/jquery-validate/additional-methods.js') }}"></script>
    <script src="{{ asset('libs/lightslider/js/lightslider.min.js') }}"></script>
    <script src="{{ asset('libs/parallax.min.js') }}"></script>
	<script src="{{ asset('libs/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

</body>
</html>