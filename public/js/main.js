$(function () {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	/*Validation Form*/
	var app_form = $('#application_form');
	app_form.validate({
		rules: {
			name: {
				required: true
			},
			phone: {
				required: true
			},
			email: {
				required: true
			},
			terms: {
				required: true
			},
		}
	});

	$('#send_btn').on('click', function () {

		if (app_form.valid()) {
			var name = $('[name="name"]').val(),
				phone = $('[name="phone"]').val(),
				email = $('[name="email"]').val(),
				form = $(this).parents().eq(1);

			$.ajax({
				url: '/send-email',
				method: 'POST',
				data : {
					name: name,
					phone: phone,
					email: email
				}
			}).done(function () {
				console.log('done');
				swal('Succes', 'Aplicarea a fost transmisa.', 'success');

				form.find('input').val('');

			}).fail(function () {
				console.log('FAIL');
			});
			console.log('VALID');
		} else {
			console.log('INVALID');
		}

	});

	$('.main_nav li a, .little_nav_bar li a').on('click', function () {
		var go_to = $(this).attr('href');
		$([document.documentElement, document.body]).animate({
			scrollTop: $(go_to).offset().top - 150
		}, 1000);

		$('.little_nav_bar').fadeOut();
	});

	/*Module: Little Menu*/
	$('.menu_btn').on('click', function () {
		$('.navbar_little').removeClass('d-none');
	});

	$('.navbar_close').on('click', function () {
		$('.navbar_little').addClass('d-none');
	});


	/*MODULE: Js Changing Dynamic shadow*/

	$('.consult_btn').addClass('shadow-pulse');
	$('.consult_btn_blue').addClass('shadow-pulse-blue');

	var counter = 0;
	setInterval(function () {
		if (counter == 0) {
			$('.consult_btn').addClass('shadow-pulse');
			$('.consult_btn_blue').addClass('shadow-pulse-blue');
			counter++;
		} else {
			$('.consult_btn').removeClass('shadow-pulse');
			$('.consult_btn_blue').removeClass('shadow-pulse-blue');
			counter--;
		}
	}, 1500);

	/*MODULE: Jquery Scroll*/

	$('.little_nav').fadeOut();

	$(document).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll > 300) {
			$('.little_nav').removeClass('cS-hidden');
			$('.little_nav').fadeIn();
		} else {
			$('.little_nav').fadeOut();
		}
	});

	/*MODULE: Redirect to homepage on logo click*/
	$('.main_logo').on('click', function () {
		window.location.href = '/';
	});

	/*Module: Show hidden little navbar*/
	$('.show_little_nav').on('click', function () {
		$('.little_nav_bar').fadeIn();
	});

	$('.close_little_btn').on('click', 'i', function () {
		$('.little_nav_bar').fadeOut();
	});

	/*MODULE: Form Validation*/

	var messages, lang = $.cookie("lang");

	if (lang === 'ru') {
		messages = {
			popup: {
				title: 'Успех',
				content: 'Заявка подана.'
			},
			form : {
				name_req: 'Имя обязательно',
				phone_req: 'Телефон обязательно',
				terms_req: 'Условия обязательно'
			}
		};
	} else {
		messages = {
			popup: {
				title: 'Succes',
				content: 'Aplicarea a fost transmisa.'
			},
			form : {
				name_req: 'Numele obligatoriu',
				phone_req: 'Telefonul obligatoriu',
				terms_req: 'Termenii obligatoriu'
			}
		};
	}


	var app_form = $('.form_apply');

	/*MODULE: Form apply*/
	app_form.on('submit', function () {
		var form = $(this), data;

		form.validate({
			rules: {
				name: {
					required: true
				},
				phone: {
					required: true
				},
				terms: {
					required: true
				}
			},
			messages : {
				name : {
					required: messages.form.name_req
				},
				phone: {
					required: messages.form.phone_req
				},
				terms: {
					required: messages.form.terms_req
				}
			}
		});

		if (form.valid()) {
			$('.close').click();
			swal(messages.popup.title, messages.popup.content, 'success');
			data = form.serialize();
			$.ajax({
				url: '/api/send-email',
				method: 'post',
				data
			}).done(function () {
				console.log('DONE');
				form.find('input').val('');
			}).fail(function () {
				console.log('FAIL');
			});

		} else {
			console.log('not valid');
		}
		return false;
	});

	/*Module: BTN Scroll down*/

	//TODO: Create btn scroll down

	// $('#section5').click(function (e) {
	//     e.preventDefault();
	//     $('html, body').animate({
	//         scrollTop: $("#menu-main").position().top
	//     }, 1000);
	// });
	//
	// $('.item').on('click', function () {
	//     var nr = $(this).attr('nr');
	//
	//     $.ajax({
	//         url: 'get-text/' + nr
	//     }).done(function (resposne) {
	//         console.log(resposne);
	//     }).fail(function () {
	//         console.log('error');
	//     });
	//
	// });
});



