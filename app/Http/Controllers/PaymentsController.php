<?php

namespace App\Http\Controllers;
use App\Model\admin\Customer;

use App\LanguageStore;
use Illuminate\Http\Request;
use App\http\Requests;
use App\Model\admin\Settings;

class PaymentsController extends Controller
{
  public function success(Request $request)
  {
	  $customer = new Customer;
	  $customer->status     = $request->ik_inv_st;
	  $customer->ik_inv_id  = $request->ik_inv_id;
	  $customer->ik_inv_st 	= $request->ik_inv_st;
	  $customer->ik_pm_no 	= $request->ik_pm_no;
	  $customer->save();

	  //return $request->all();
	  $settings = Settings::find('1');
	  $lang = lang();
	  $text = LanguageStore::pageText($lang);

	  return view('success.index', get_defined_vars());
  }
	
  public function status(Request $request)
  {
	$customer = Customer::where('ik_inv_id', $request->ik_inv_id)->first();
	$customer->ik_inv_st  = $request->ik_inv_st;
	$customer->status = $request->ik_inv_st;
	$customer->save();  
	  
    return $request->all();
  }
	
  public function save(Request $request)
  {
	  $customer = Customer::where('ik_inv_id', $request->ik_inv_id)->first();
	  $customer->name  = $request->name;
	  $customer->phone = $request->phone;
	  $customer->msg   = $request->problem;
	  $customer->save();
	  
	  return response()->json($customer);
  }	
}
