<?php

namespace App\Http\Controllers\Admin;

use App\Model\admin\Settings;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = settings::paginate(10);
        return view('admin.settings.show', compact('settings'));
    }

    public function editSettings(request $request){
      $rules = array(
        'lei'  => 'required',
    	  'rub' => 'required',
        'desc' => 'required'
    	);
    	$validator = Validator::make ( Input::all(), $rules);
    	if ($validator->fails())
    		return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    	else {
    		$settings = Settings::find ($request->id);
        $settings->lei     = $request->lei;
    		$settings->rub     = $request->rub;
    		$settings->save();
    		return response()->json($settings);
    	}
    }
}
