<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\admin\Posts;
use Illuminate\Http\Request;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\http\Requests;
use Illuminate\Support\Facades\Auth;
use Image;

class PostController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getPost(Request $request)
    {
      $post = Posts::find($request->id);
      return response()->json($post);
    }

    public function imagePost(Request $request)
    {
      $validation = Validator::make($request->all(), [
        'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
       ]);

        $post = Posts::find ($request->id);

        if($validation->passes())
       {
         $image = $request->file('select_file');
         $filename = time() . '.' . $image->getClientOriginalExtension();
         $location = public_path('images/posts/'.$filename);
         Image::make($image)->resize(700, 400)->save($location);

         $post->image =  $filename;
         $post->save();

         return response()->json([
            'id' => $post->id,
            'status' => 'success',
            'message'   => 'Image Upload Successfully',
            'uploaded_image' => '<img src="/images/posts/'. $filename .'" width="200px" />',
            'class_name'  => 'alert-success'
          ]);
       }
       else
       {
        return response()->json([
          'id' => $post->id,
          'status' => 'error',
          'message'   => 'Image Upload Error',
          'uploaded_image' => '',
          'class_name'  => 'alert-danger'
        ]);
       }
    }

    public function addPost(Request $request)
    {
    	$rules = array(
        'slug'  => 'required',
        'title_ro' => 'required',
        'title_ru' => 'required',
        'title_en' => 'required',
        'body_ro' => 'required',
        'body_ru' => 'required',
        'body_en' => 'required'
        //'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
    	);
    	$validator = Validator::make ( Input::all(), $rules);
    	if ($validator->fails())
    		return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));
    	else {
            $post = new Posts;
            $post->slug = $request->slug;
            $post->title_ro = $request->title_ro;
            $post->title_ru = $request->title_ru;
            $post->title_en = $request->title_en;
            $post->body_ro = $request->body_ro;
            $post->body_ru = $request->body_ru;
            $post->body_en = $request->body_en;
            $post->image = 'default.png';

            /*$image = $request->file('select_file');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/posts/'.$filename);
            Image::make($image)->resize(700, 400)->save($location);
            $post->image = $filename;*/

            $post->save();

    		return response()->json($post);
    	}
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->can('posts.create')) {
           $tags =tag::all();
            $categories =category::all();
            return view('admin.post.post',compact('tags','categories'));
        }
        return redirect(route('admin.home'));

    }

    public function editPost(request $request){
      $rules = array(
        'slug'  => 'required',
    	  'title_ro' => 'required',
        'title_ru' => 'required',
        'title_en' => 'required',
    	  'body_ro' => 'required',
        'body_ru' => 'required',
        'body_en' => 'required'
        //'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
    	);
    	$validator = Validator::make ( Input::all(), $rules);
    	if ($validator->fails())
    		return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

    	else {
    		$post = Posts::find ($request->id);
        $post->slug     = $request->slug;
    		$post->title_ro = $request->title_ro;
        $post->title_ru = $request->title_ru;
        $post->title_en = $request->title_en;
    		$post->body_ro  = $request->body_ro;
        $post->body_ru  = $request->body_ru;
        $post->body_en  = $request->body_en;
        /*
        $image = $request->file('select_file');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $location = public_path('images/posts/'.$filename);
        Image::make($image)->resize(700, 400)->save($location);

        $post->image = $filename;
        */
    		$post->save();
    		return response()->json($post);
    	}
    }

    public function deletePost(request $request){
    	$post = Posts::find ($request->id)->delete();
    	return response()->json();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'subtitle' => 'required',
            'slug' => 'required',
            'body' => 'required',
            'image' => 'required',
            ]);
        if ($request->hasFile('image')) {
            $imageName = $request->image->store('public');
        } else {
            return 'No';
        }
        $post = new post;
        $post->image = $imageName;
        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->slug = $request->slug;
        $post->body = $request->body;
        $post->status = $request->status;
        $post->save();
        $post->tags()->sync($request->tags);
        $post->categories()->sync($request->categories);

        return redirect(route('post.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('posts.update')) {
            $post = post::with('tags','categories')->where('id',$id)->first();
            $tags =tag::all();
            $categories =category::all();
            return view('admin.post.edit',compact('tags','categories','post'));
        }
        return redirect(route('admin.home'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'subtitle' => 'required',
            'slug' => 'required',
            'body' => 'required',
            'image'=>'required'
            ]);
        if ($request->hasFile('image')) {
            $imageName = $request->image->store('public');
        }
        $post = post::find($id);
        $post->image = $imageName;
        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->slug = $request->slug;
        $post->body = $request->body;
        $post->status = $request->status;
        $post->tags()->sync($request->tags);
        $post->categories()->sync($request->categories);
        $post->save();

        return redirect(route('post.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        post::where('id',$id)->delete();
        return redirect()->back();
    }
}
