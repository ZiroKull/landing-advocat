<?php


namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Model\admin\Customer;
use Illuminate\Http\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class CustomerApiController extends ApiController
{
    private $token = '3AJRQzr3HFic9p0hpaX7oUG6xgBohr94gwbnEzbcQ0=';

    public function sendNumbers(Request $request)
    {
        $token = $request->get('token');

        if ($token == $this->token) {
            $numbers = explode(',', $request->get('numbers'));

            foreach ($numbers as $number) {
                $phone_existence = Customer::where('phone', trim($number))->first();
                if (is_null($phone_existence)) {
                    Customer::create([
                        'phone' => trim($number),
                        'name' => 'Client Aplicatie mobila',
                        'problem' => 'Nu este informatie',
                        'status' => 'Success'
                    ]);
                }
            }

            return json_encode(['status' => 'success']);
        }

        return json_encode(['status' => 'error']);
    }


//    public function makeLog()
//    {
//        $log = new Logger('api_feedback');
//        $log->pushHandler(new StreamHandler(public_path('/'), Logger::WARNING));
//        $log->warning('Foo');
//        $log->error('Bar');
//    }
}
