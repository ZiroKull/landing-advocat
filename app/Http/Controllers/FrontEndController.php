<?php
namespace App\Http\Controllers;


use App\LanguageStore;
use App\Model\admin\Customer;
use App\Model\admin\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontEndController extends Controller
{
    public function index()
    {
        $lang = lang();
        $text = LanguageStore::pageText($lang);

		$yandex_lang = 'ro_RO';
		if ($lang == 'ru') {
			$yandex_lang = 'ru_RU';
		}

        $posts = Posts::orderBy('id', 'desc')->limit(3)->get()->toArray();

        return view('front/index', compact('text', 'lang', 'yandex_lang', 'posts'));
    }

    public function sendEmail(Request $request)
    {
        $data['data'] = $request->all();

        Mail::send('mail', $data, function($message) {
            $message->to('info@avocat-moldova.md', 'Aplicare website')->subject('Aplicare');
            $message->from('info@avocat-moldova.md', 'Client');
        });

        $phone_existence = Customer::where('phone', $request->get('phone'))->first();
        if (is_null($phone_existence)) {
            /*TODO: Send the problem*/
            Customer::create([
                'phone' => $request->get('phone'),
                'name' => $request->get('name'),
                'status' => 'Landing Page Client',
                'problem' => $request->get('problem') ?? 'Nu este informatie'
            ]);
        } else {
            $customer = Customer::where('phone', $request->get('phone'))->first();
            $customer->name  = $request->get('name');
            $customer->phone = $request->get('phone');
            $customer->save();
        }
    }
}