<?php

namespace App\Http\Controllers;

use App\LanguageStore;
use App\Model\admin\Settings;
use Illuminate\Http\Request;

class TermsController extends Controller
{
    public function index()
    {
        $settings = Settings::find('1');
        $lang = lang();
        $text = LanguageStore::pageText($lang);

        $yandex_lang = ($lang == 'RU') ? 'ru_RU' : 'ro_RO';

        return view('terms.index', get_defined_vars());
    }
}
