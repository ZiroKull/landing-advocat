<?php

namespace App\Http\Controllers;

use App\LanguageStore;
use Illuminate\Http\Request;
use App\Model\admin\Posts;
use App\Model\admin\Settings;

class BlogControllers extends Controller
{
  public function index()
  {
	  $settings = Settings::find('1');
      $posts = Posts::orderBy('id', 'DESC')->paginate(3);
      $lang = lang();
      $text = LanguageStore::pageText($lang);

      $yandex_lang = ($lang == 'RU') ? 'ru_RU' : 'ro_RO';

      return view('blog.index', get_defined_vars());
  }

  public function getSingle($slug)
  {
	$settings = Settings::find('1');
    $post = Posts::where('slug', '=', $slug)->first()->toArray();
    $lang = lang();
    $text = LanguageStore::pageText($lang);

    $yandex_lang = ($lang == 'RU') ? 'ru_RU' : 'ro_RO';

    return view('blog.post', get_defined_vars());

    return $slug;
  }
}
