<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\admin\Posts;
use Illuminate\Http\Request;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use App\http\Requests;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $posts = Posts::paginate(10);
      return view('admin/home',compact('posts'));
    }
}
