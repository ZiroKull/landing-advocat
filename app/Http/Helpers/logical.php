<?php
/**
 * Set && Check current language
 */
function lang($default = 'ro')
{
    if (isset($_GET['lang']) && !empty($_GET['lang'])) {
        $lang = $_GET['lang'];
        $_SESSION['lang'] = $lang;
        setcookie('lang', $lang, time() + (3600 * 24 * 30));
    } elseif (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
    } elseif (isset($_COOKIE['lang'])) {
        $lang = $_COOKIE['lang'];
    } else {
        $lang = $default;
    }
    return $lang;
}