<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LanguageStore extends Model
{
    public static function pageText($lang)
    {
        $text = [];

        if ($lang == 'ro') {

            $text['menu'] = [
                'home' => 'ACASĂ',
                'help' => 'Domenii de specializare',
                'about_us' => 'DESPRE NOI',
                'domains' => 'DOMENII DE ACTIVITATE',
                'contacts' => 'CONTACTE',
                'blog'    => 'BLOG'
            ];

            $text['terms'] = [
                'title' => 'Politica raportului de colaborare',
                'content' => [
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Raportul de colaborare dintre Dumneavoastră (dvs.) în calitate de Beneficiar și Portalul Online “Competent.md” reglementează mecanismul de oferire a consultanței juridice profesionale. Acceptarea regulilor de colaborare se face prin accesarea, solicitarea, achitarea serviciilor prestate de către Portalul Online “Competent.md”. În cazul în care nu doriți să acceptați politica raportului de colaborare nu accesați, nu solicitați și nu achitați serviciile Portalului Online “Competent.md”. Prezentul acord de colaborare se aplică fără legalizare suplimentară, în conformitate cu legislația Republicii Moldova.</p>',
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Portalul Online “Competent.md” se obligă să presteze Beneficiarului servicii de consultanță juridică profesională în conformitate cu comanda înregistrată și acceptată prin înaintarea ofertei, iar Beneficiarul se obligă să efectueze plata convenită la termenul și condițiile prevăzute de ofertă.</p>',
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Oferta va conține termenul de executare și suma retribuției. Ea poate fi acceptată prin depunerea banilor pe contul Portalului Online “Competent.md” în termen de cel mult 15 zile de la efectuarea pasului 2 a procedurii de colaborare (vezi procedura de colaborare).</p>',
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Raportul de colaborare dintre Beneficiar și Portalul Online “Competent.md” se va reglementa după următoarea procedură:</p>',
                    '<p style="text-indent: 40px; text-align: justify; padding-bottom: inherit;">Pasul 1. Benificiarul va face comanda prin accesarea Portalului Online “Competent.md” unde va descrie în detaliu esența problemei și doleanţa faţă de actele expediate site-ului “Competent.md”, va selecta metoda de plată și perioada de timp în care dorește să obțină răspunsul;</p>',
                    '<p style="text-indent: 40px; text-align: justify; padding-bottom: inherit;">Pasul 2. Beneficiarul va aștepta confirmarea comenzii. La adresa de email a Beneficiarului va fi expediat un mesaj cu indicarea costul serviciilor, termenii și datele bancare ale Portalului Online “Competent.md”;</p>',
                    '<p style="text-indent: 40px; text-align: justify; padding-bottom: inherit;">Pasul 3. Beneficiarul va efectua plata şi va confirma efectuarea plăţii printr-un e-mail. Portalul Online “Competent.md” va începe prestarea serviciilor din data recepționării avansului în mărime de 50% din costul real al consultării, partea rămasă va fi plătită odată cu recepţionarea actelor sau serviciilor solicitate;</p>',
                    '<p style="text-indent: 40px; text-align: justify;">Pasul 4. Actele finale vor fi expediate la adresa de email a Beneficiarului în perioada de timp solicitată. Vom răspunde cu promptitudine la orice întrebare în limita serviciului acordat.</p>',
                ]
            ];

            $text['help_with'] = [
                'title' => 'VĂ AJUTĂM CU',
                'title2' => 'Blog',
                1 => 'Consultanță rapidă',
                2 => 'Servicii publice',
                3 => 'Afaceri (business)',
                4 => 'Asistență pe cauze penale in domeniul afacerilor',
                5 => 'Imobiliare',
                6 => 'Recuperare datorii (creanțe)',
                7 => 'Compensare daune și <br/> asistență pasagerilor aerieni
',
                8 => 'Despăgubiri accidente rutiere',
                9 => 'Compensare daune și asistență <br/> în muncă a emigranților',
				10 => 'Consultanță',
            ];

            $text['deviza'] = [
                'Luptăm împreună,',
                'câștigăm împreună'
            ];

            $text['about_us'] = [
                'title' => 'DESPRE NOI',
                'subtitle' => 'avocat, doctor în drept',
                'description' => 'Cabinetul avocatului ,,Veaceslav Spalatu” este fondat în anul 2007 și activează pe piața avocaturii mai bine de 10 ani. Cabinetul nostru de avocatură are menirea exclusivă de a vă ajuta în orice problemă juridică apărută; de a vă consulta, reprezenta, asista în chestiuni juridice de orice natură, în fața oricăror instituții, organizații, întreprinderi naționale și internaționale indiferent de forma de proprietate a lor; în fața organelor ale gestiunii administrative și de stat a Republicii Moldova și a altor state ca subiecte de drept internațional, precum și în fața organelor de poliție, ale procuraturii, instanțelor de judecată, instituțiilor de executare naționale și din alte state, etc., cu toate împuternicirile reprezentantului prevăzute de legislația în vigoare a Republicii Moldova.',
                'principles' => [
                    'title' => 'Principii fundamentale de cooperare',
                    'professionalism' => 'PROFESIONALIZM',
                    'correct' => 'CORECTITUDINE',
                    'trust' => 'ÎNCREDERE',
                    'punctuality' => 'PUNCTUALITATE',
                    'responsability' => 'RESPONSABILITATE',
                    'comunicability' => 'COMUNICABILITATE',
                ]
            ];

            $text['domains'] = [
                'title' => 'DOMENII DE ACTIVITATE',
                'Dreptul afacerilor (business)',
                'Dreptul civil',
                'Dreptul familiei',
                'Dreptul muncii și protecției <br/> sociale',
                'Dreptul protecției <br/>intelectuale',
                'Drept <br/> migrațional',
                'Dreptul contenciosului <br/>administrativ',
                'Dreptul penal',
                'Executarea hotărîrilor <br/>judecătorești',
                'Curtea Europeană a<br/>Drepturilor<br>Omului (CEDO)',
            ];

            $text['titles'] = [
                'partners' => 'PARTENERI',
                'find_us' => 'GĂSIȚI-NE PE HARTĂ',
                'contacts' => 'CONTACTE',
                'form_title' => 'Descrieți problema',
                'form_subtitle' => 'și noi găsim soluția!',
            ];

            $text['form'] = [
                'title' => 'consultanță RAPIDĂ',
                'title_pay' => 'Puteți avea parte de o consultație online apăsând butonul de mai jos.',
                'name' => 'Nume',
                'phone' => 'Telefon',
                'problem' => 'Descrieti problema',
                'btn_consult' => 'consultanță RAPIDĂ',
                'btn_send' => 'Transmite',
                'btn_pay'  => 'Comandă acum!',
                'terms1'    => 'Sunt de acord cu ',
                'terms2'    => '<br>termenii și condițiile'
            ];

            $text['blog'] = [
                'more' => 'Mai mult',
                'articles' => 'Toate articolele'
            ];

            $text['contacts'] = [
                'street' => 'str. Cuza Vodă, 31/1 mun. Chișinău'
            ];

            /*HELP WITH MODALS*/

            $text['help_with_modals'] = [
                'free_consult' => [
                    'title' => 'Consultanță rapidă',
                    'intro' => 'Pentru diverse problematici și întrebări de ordin juridic cu complexitate moderată, noi acordăm gratuit:',
                    'sub_intro' => [
                        'consultanță juridică on-line.',
                        'consultanță juridică telefonică.',
                        'consultanță juridică în oficiu.',
                    ],
                    'ending' => 'Pentru problemele cu nivel mediu / avansat de compexitate, consultanța juridică solicitată va fi cu titlu oneros (cu plată). În astfel de situații, se invită persoanele cointeresate la sediul nostru, pentru analiza complexă a problemelor Dvs. și identificarea unor soluții optime.'
                ],

                'public_service' => [
                    'title' => 'Servicii publice',
                    'intro' => 'În domeniul serviciilor publice noi acordăm consultanță juridică și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'obținerea actelor de stare civilă (naștere, căsătorie, divorț, etc).',
                        'înregistrarea și licențierea companiilor și a altor unități de drept.',
                        'înmatricularea vehiculelor importate; identificarea vehiculului; transcrierea transmiterii dreptului de proprietate; comodat, locațiune, leasing.'
                    ],
                ],

                'business' => [
                    'title' => 'Afaceri (business)',
                    'intro' => 'În domeniul dreptului afacerilor noi acordăm consultanță juridică și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'întocmire, redactarea și expertizarea contractelor comerciale.',
                        'asistență la negocieri și tranzacții în vederea încheierii contractelor comerciale.',
                        'litigii referitoare la încălcarea obligațiilor contractuale, reprezentare în fața instanțelor de judecată de drept comun și de arbitraj.',
                        'societăți comerciale – constituiri, consiliere, regim juridic aplicabil (înregistrare, licențiere, înregistrare a modificărilor operate în actele de constituire ale întreprinderii, reorganizare, suspendarea activității, lichidarea persoanelor juridice).',
                        'recuperări de debite – executare silită, somații de plată, concilieri.'
                    ]
                ],

                'business_assistence' => [
                    'title' => 'Asisitență pe cauze penale în domeniul afacerilor',
                    'intro' => 'De multe ori planurile ori proiectele de afaceri ale unor companii riscă să depășească cadrul legal, prin urmare, activitatea acestora pot fi în contradicţie cu legile penale. Pentru prevenirea unor astfel de situaţii, noi acordăm consultanță juridică și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'întocmiri de cereri, demersuri, plîngeri, etc.',
                        'întocmiri de acțiuni civile (recuperare prejudicii și daune interese) în cauze penale.',
                        'reprezentare și asistentă în fața organelor de urmărire penală.',
                        'înaintarea de obiecţii împotriva acţiunilor organului de urmărire penală.',
                        'contestarea în modul stabilit de lege, a acţiunilor şi hotărîrilor organului de urmărire penală.',
                        'reprezentare și asistentă în fața instanțelor de judecată.',
                    ]
                ],

                'imobiles' => [
                    'title' => 'Imobiliare',
                    'intro' => 'În domeniul serviciilor imobiliare, noi acordăm consultanță juridică și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'elaborare de contracte, alte acte juridice privitor la: vanzarea-cumpararea bunurilor imobile; locațiune (chirie) de oficii, apartamente; arendă de spații comerciale.',
                        'sub_list' => [
                            'title' => 'analiza, pregătirea documentației referitoare la oferta de:',
                            'vînzare-cumpărare a imobilelor.',
                            'locațiune (chirie) de apartamente, oficii.',
                            'arenda spațiilor comerciale.',
                        ],
                        'reprezentare la tranzacțiile de vînzare-cumpărare a imobilelor (bunuri industriale, case de locuit, apartamente, spații comerciale, oficii, terenuri pentru construcții, terenuri arabile, etc.',
                        'reprezentare la tranzacțiile de locațiune (chirie) de apartamente, oficii; arendă a spațiilor comerciale, etc.',
                        'reprezentare în fața organelor de drept și a instanțelor de judecată pe litigii imobiliare.'
                    ]
                ],

                'debts' => [
                    'title' => 'Recuperare datorii (creanțe)',
                    'intro' => 'În domeniul recuperării datorii (creanțe), noi acordăm consultanță juridică și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'împrumuturi nerestituite la termen (în baza recipiselor, contractelor de împrumut etc.).',
                        'plata drepturilor salariale neachitate de angajator în termenul contractual.',
                        'sume bănești datorate în baza contractelor de locațiune (chirie).',
                        'sume bănești datorate ca urmare a prestării de servicii.',
                        'sume bănești restante ca urmare a încheierii tranzacțiilor de vînzare-cumpărare și a altor tipuri de tranzacții.',
                        'sume bănești achitate de Cumpărător (consumator) pentru produse, servicii necorespunzătoare.',
                    ]
                ],

                'avia' => [
                    'title' => 'Compensare daune și asistență pasagerilor aerieni',
                    'intro' => 'În acest domeniu, noi acordăm consultanță juridică și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'întocmiri de interpelări, plîngeri, somații, cereri de chemare în judecată etc.',
                        'sub_list' => [
                            'title' => 'reprezentare și asistentă juridică în fața:',
                            'companiilor aeriene naționale.',
                            'instituțiilor și organizațiilor de stat / private din Republca Moldova.',
                            'organelor de drept și a instanțelor de judecată.',
                        ],
                        'sub_list_2' => [
                            'title' => 'compensarea daunelor materiale și morale în legătură cu:',
                            'întîrzierea și anularea zborurilor.',
                            'pierderea și deteriorarea bagajelor.',
                            'organelor de drept și a instanțelor de judecată.',
                        ],
                    ]
                ],

                'transport' => [
                    'title' => 'Despăgubiri accidente rutiere',
                    'intro' => 'În domeniul despăgubiri accidente rutiere, noi acordăm consultanță juridică și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'întocmiri de cereri, plîngeri, somații, cereri de chemare în judecată etc.',
                        'sub_list' => [
                            'title' => 'reprezentare și asistentă juridică în fața:',
                            'asiguratorilor auto licențiați din Republica Moldova.',
                            'Centrului Naţional de Expertize Judiciare de pe lîngă Ministerul Justiţiei al Republicii Moldova.',
                            'Centrelor de expertiză independente din Republica Moldova.',
                            'Biroului Național al Asigurătorilor de Autovehicule din Republica Moldova.',
                            'altor instituții și organizații de stat / private din Republca Moldova.',
                            'organelor de drept și a instanțelor de judecată.',
                        ],
                    ]
                ],

                'emigrants' => [
                    'title' => 'Compensare daune și asistență în muncă a emigranților',
                    'intro' => 'În domeniul migrației de muncă noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'analiza ofertelor de muncă peste hotare acordate de agențiile private de ocupare a forței de muncă din Republica Moldova.',
                        'consultanță și asistență la încheierea contractelor de mediere privind plasarea la muncă în străinătate și a altor acte juridice.',
                        'recuperare prejudicii materiale, morale, daune interese pentru încălcarea obligațiunilor contractuale de către agențiile private de ocupare a forței de muncă din Republica Moldova.',
                        'integrarea membrilor de familii în societatea statului de emigrare (loc de muncă, gradiniță, școală, servicii sociale, servicii medicale, integrare culturală etc.).',
                    ]
                ],
            ];

            $text['activity_modals'] = [
                'business_right' => [
                    'title' => 'Dreptul afacerilor (business)',
                    'intro' => 'În domeniul dreptului afacerilor noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'întocmire, redactarea și expertizarea contractelor comerciale.',
                        'asistență la negocieri și tranzacții în vederea încheierii contractelor comerciale.',
                        'litigii referitoare la încălcarea obligațiilor contractuale, reprezentare în fața instanțelor de judecată de drept comun și de arbitraj.',
                        'societăți comerciale – constituiri, consiliere, regim juridic aplicabil <i>(înregistrare, licențiere, înregistrare a modificărilor operate în actele de constituire ale întreprinderii, reorganizare, suspendarea activității, lichidarea persoanelor juridice).</i>',
                        'recuperări de debite – executare silită, somații de plată, concilieri.'
                    ]
                ],

                'civil_right' => [
                    'title' => 'Dreptul civil',
                    'intro' => 'În domeniul dreptului civil noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'întocmirea, redactarea și încetarea contractelor.',
                        'partaj <i>(împărțire)</i> judiciar.',
                        'succesiuni <i>(transmitere de drepturi și obligații)</i>.',
                        'răspundere civilă delictuală și contractuală.',
                        'acțiuni în nulitatea actelor.',
                        'acțiuni privind dobîndirea dreptului de proprietate.',
                        'acțiuni în pretenții.',
                        'acțiuni în posesii.',
                        'moștenire, partaj moștenire, acțiuni pentru întrarea în posesia unei mosteniri.',
                        'întocmirea și redactarea cererilor de chemare în judecată, de apel, de recurs, a notificărilor, pretențiilor, alte cereri, a tranzacțiilor, alte acte cu caracter juridic.',
                        'asistență juridică și reprezentare în fața instanței de fond, Curții de Apel, Curții Supreme de Justiție, Curții de Arbitraj Comercial Internațional din Chișinău.',
                        'măsuri asiguratorii.',
                        'asistență în procedura de executare a hotărîrilor judecătorești.',
                        'asistență si reprezentare în fața administrațiilor publice, organelor de drept și notarului public.',
                    ]
                ],

                'family_right' => [
                    'title' => 'Dreptul familiei',
                    'intro' => 'În domeniul dreptului familiei noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'litigii',
                        'divorț.',
                        'anularea căsătoriei.',
                        'partajul averii soților.',
                        'decăderea din drepturile părintești.',
                        'adopțe.',
                        'stabilire pensie de întreținere.',
                        'consultanță juridică curentă.',
                        'mediere conflicte, conciliere.',
                        'asistență juridică și reprezentare în fața notarului public.',
                        'asistență juridică și reprezentare în raporturile cu organe ale administrației publice.',
                        'asistență juridică și reprezentare în raporturile cu orice persoane fizice sau juridice.',
                    ]
                ],

                'work_right' => [
                    'title' => 'Dreptul muncii și protecției sociale',
                    'intro' => 'În domeniul dreptului muncii protecției sociale, noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'întocmirea, perfectarea și încetarea contractelor de muncă.',
                        'negocierea clauzelor specifice: clauze de non-concurenţă, clauze de confidenţialitate, clauze de mobilitate, etc..',
                        'asistență la încetarea raporturilor juridice de muncă <i>(prin demisie, concedieri etc.)</i>. ',
                        'asistența în litigiile <i>(conflictele)</i> de muncă în fața instituțiilor de stat / private și a instanțelor de judecată.',
                    ]
                ],

                'intelectual_right' => [
                    'title' => 'Dreptul protecției intelectuale',
                    'intro' => 'În domeniul dreptului protecției intelectuale, noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'sub_list' => [
                            'title' => 'asistență și reprezentarea în legătură cu:',
                            'protecția proprietății intelectuale și a drepturilor de autor.',
                            'încheierea contractelor specifice.',
                            'înregistrarea și protecția juridică a mărcii.',
                            'prevenirea și combaterea fapteor de concurență neloială și obținerea de despăgubiri.',
                            'litigii civile și cauze penale privind concurența neloială, încălcarea drepturilor de autor și a drepturilor conexe.',
                        ]
                    ]
                ],

                'migrational_right' => [
                    'title' => 'Drept migrațional',
                    'intro' => 'În domeniul dreptului migrațional noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'sub_list' => [
                            'title' => 'pentru emigranții din Republica Modova:',
                            'analiza ofertelor de muncă peste hotare acordate de agențiile private de ocupare a forței de muncă din Republica Moldova.',
                            'consultanță și asistență la încheierea contractelor de mediere privind plasarea la muncă în străinătate ș ia altor acte juridice.',
                            'recuperare prejudicii materiale, morale, daune interese pentru încălcarea obligațiunilor contractuale de către agențiile private de ocupare a forței de muncă din Republica Moldova.',
                            'integrarea membrilor de familii în societatea statului de emigrare (loc de muncă, gradiniță, școală, servicii sociale, servicii medicale, integrare culturală).',
                        ],
                        'sub_list2' => [
                            'title' => 'pentru imigranții în Republica Moldova:',
                            'asisntență în obținerea permisului de şedere provizorie.',
                            'asistență în fața Biroului Migrație și Azil al MAI în vederea obținerii permisului de ședere.',
                            'asistență pentru plasarea în cîmpul muncii.',
                            'asistență pentru înmatricularea la studii.',
                            'asistență pentru procurarea/încherierea unei locuințe.',
                            'asistenta privind deschiderea de afaceri peteritoriul Republicii Moldova.',
                            'asistență în vederea obținerii temporare a plăcuțelor de înmatriculare a vehiculelor de transport.',
                        ],
                    ]
                ],

                'administrativ_right' => [
                    'title' => 'Dreptul contenciosului administrativ',
                    'intro' => 'În domeniul contenciosului administrativ, noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'anularea actelor administrative ilegale adoptate organele administrative sau subdiviziunele unui organ administrativ de stat.',
                        'participare pe litigii de contencios administrativ în fața instituțiilor de stat și a instanțelor de judecată.'
                    ]
                ],

                'penal_right' => [
                    'title' => 'Dreptul penal',
                    'intro' => 'În domeniul dreptului penal noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'întocmiri de cereri, demersuri, plîngeri, etc.',
                        'întocmiri de acțiuni civile (recuperare prejudicii și daune interese) în cauze penale.',
                        'reprezentare și asistentă în fața organelor de urmărire penală.',
                        'înaintarea de obiecţii împotriva acţiunilor organului de urmărire penală.',
                        'contestarea în modul stabilit de lege, a acţiunilor şi hotărîrilor organului de urmărire penală.',
                        'reprezentare și asistentă în fața instanțelor de judecată.',
                    ]
                ],

                'execution_right' => [
                    'title' => 'Executarea hotărîrilor judecătorești',
                    'intro' => 'În domeniul executării hotărîrior judecătorești, noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'întocmiri de cereri, demersuri, plîngeri, etc.',
                        'ridicare de titluri executorii și intentarea procedurii de executare la cerea creditorului.',
                        'sub_list' => [
                            'title' => 'reprezentare și asistentă juridică în fața:',
                            'executorilor judecătorești la actele de executare.',
                            'instanțelor de judecată.',
                        ],
                        'asigurarea executării documentului executoriu.',
                        'recuzarea executorului judecătoresc.',
                        'contestarea actelor executorului judecătoresc.',
                        'înaintarea de obiecţii împotriva cererilor, argumentelor şi considerentelor celorlalţi participanţi la procedura de executare.',
                        'căutarea debitorului și a bunurilor sale.',
                        'concilierea părților în procedura de executare și încheierea de tranzacții.',
                    ]
                ],

                'cedo_right' => [
                    'title' => 'Curtea Europeană a Drepturilor Omului (CEDO)',
                    'intro' => 'În domeniul CEDO, noi acordăm consultanță și reprezentare pe următoarele problematici, și anume:',
                    'sub_intro' => [
                        'analiza hotărîrilor judecătorești și a altor acte juridice.',
                        'întocmirea și redactarea cererilor adresate Curții Europene a Drepturilor Omului (CEDO), precum și a altor acte juridice de procedură.'
                    ]

                ]
            ];

            $text['tabs'] = [
                'tab1' => 'Achită online',
                'tab2' => 'Carduri bancare',
                'tab3' => 'Terminale self-service',
                'tab4' => 'Bani în numerar',
                'desc2' => 'Puteți achita serviciile noastre fără comision prin intermediul cardurilor bancare Visa sau Mastercard.',
                'desc3' => 'Terminalele QIWI amplasate în locurile de acces general.',
                'desc4' => 'Achitați serviciile direct în oficiul companiei.',
                'cabinet' => 'Cabinetul avocatului "Veaceslav Spalatu"',
                'adress'  => 'Mun. Chișinău, str. Cuza Vodă, 31/1, et. 3, of. nr. 8',
                'mcart_title' => 'VEACESLAV SPALATU',
                'mcart_numb' => '3102 1800 6051 7014',
                'mcart_acc' => '22592051520',
                'mcart_bank' => 'MAIB MasterCard',
                'qiwi' => 'qiwi: +37379998902'
            ];

        } else if ($lang == 'en') {

            $text['menu'] = [
                'home' => 'HOME',
                'help' => 'Fields of specialization',
                'about_us' => 'ABOUT US',
                'domains' => 'AREAS OF ACTIVITY',
                'contacts' => 'CONTACTS',
                'blog'     => 'BLOG'
            ];

            $text['terms'] = [
                'title' => 'Collaboration report policy',
                'content' => [
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">The collaboration report between you as Beneficiary and Internet resource Competent.md regulates the mechanism of providing professional legal advice. The acceptance of collaboration rules is performed by access, request, payment for services provided by Internet resource Competent.md. If you do not accept the policy of collaboration report, do not access, don`t seek payment towards Competent.md Internet resources services. This collaboration agreement is applied without further legalization, applicable under Moldovan law .</p>',
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Internet Resource Competent.md undertakes to provide the Beneficiary with professional legal advice in accordance with the registered and accepted order by submitting the offer and the recipient is obliged to pay under the agreed deadline and conditions of the offer.</p>',
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">The offer will contain the execution time and the amount of remuneration. It can be accepted by depositing money on Internet Resource Competent.md account within 15 days after completion of step 2 of the procedure of cooperation (see procedure for cooperation ).</p>',
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Collaboration report between the Beneficiary the Internet Resource Competent.md will be regulad by the following procedure:</p>',
                    '<p style="text-indent: 40px; text-align: justify; padding-bottom: inherit;">Step 1. The Beneficiary will make the order by accessing the Internet Resource Competent.md, where will describe in details the essence of the problem and the request over the attached documents, will select the payment method and the time limits within which he/she wishes to receve an answer;</p>',
                    '<p style="text-indent: 40px; text-align: justify; padding-bottom: inherit;">Step 2. The Beneficiary will wait the confirmation of the order. At the e-mail adress of the Beneficiary will arrive a message that will include the service costs, terms and bank details of the Internet Resource Competent.md;</p>',
                    '<p style="text-indent: 40px; text-align: justify; padding-bottom: inherit;">Step 3. Beneficiary will perform the order and will communicate about it to the Internet Resource Competent.md, that will start to provide the serivices on the date of the advance 50% payment receivement, and the next 50 % will be paid after the receiving of documents or services;</p>',
                    '<p style="text-indent: 40px; text-align: justify;">Step 4. The complete documents will be sent to the e-mail adress of the Beneficiary in the indicated deadline. We will answer promptly at any question in the limit of the performed service.</p>',
                ]
            ];

            $text['help_with'] = [
                'title' => 'WE HELP WITH',
                'title2' => 'Blog',
                1 => 'Fast consultations',
                2 => 'Public services',
                3 => 'Business',
                4 => 'Assistance in criminal cases in the field of business',
                5 => 'Real estate',
                6 => 'Debt Recovery (Receivables)',
                7 => 'Compensation for damage and <br/> assistance to air passengers',
                8 => 'Road accident damages',
                9 => 'Compensation for damages and assistance <br/>work emigrants',
				10 => 'Сonsultations',
            ];

            $text['deviza'] = [
                'Fight together,',
                'Win together'
            ];

            $text['about_us'] = [
                'title' => 'ABOUT US',
                'subtitle' => 'lawyer, doctor in law',
                'description' => "The lawyer's office \"Veaceslav Spalatu\" was founded in 2007 and has been working on the law market for more than 10 years. Our law office is solely responsible for helping you with any legal issues you may have; to consult, represent, assist in legal matters of any kind, in the face of any institution, organization, national and international enterprise, regardless of their ownership; in front of the administrative and state administration bodies of the Republic of Moldova and other states as subjects of international law, as well as in front of the police, the prosecutor's office, the courts of law, the national and other enforcement institutions, etc., with all the powers of the representative provided by the legislation in force in the Republic of Moldova.",
                'principles' => [
                    'title' => 'Fundamental principles of cooperation',
                    'professionalism' => 'PROFESSIONALISM',
                    'correct' => 'FAIRNESS',
                    'trust' => 'TRUST',
                    'punctuality' => 'PUNCTUALITY',
                    'responsability' => 'RESPONSIBILITY',
                    'comunicability' => 'COMMUNICABILITY',
                ]
            ];

            $text['domains'] = [
                'title' => 'AREAS OF ACTIVITY',
                'Business right',
                'Civil law',
                'Family law',
                'Labor and social <br/> protection',
                'Intellectual <br/> protection law',
                'Migration law',
                'The right of <br/> administrative litigation',
                'Criminal law',
                'Enforcement <br/> of judgments',
                'The European Court <br/> of Human Rights (ECHR)',
            ];

            $text['titles'] = [
                'partners' => 'PARTNERS',
                'find_us' => 'Find us on the map',
                'contacts' => 'CONTACTS',
                'form_title' => 'Describe the problem',
                'form_subtitle' => 'and we find the solution!'
            ];

            $text['form'] = [
                'title' => 'FAST CONSULTATION',
                'title_pay' => 'You can have an online consultation by clicking the button below.',
                'name' => 'Name',
                'phone' => 'Phone',
                'problem' => 'Describe the problem',
                'btn_consult' => 'fast consultation',
                'btn_send' => 'Send',
                'btn_pay'  => 'Get Consultation!',
                'terms1'    => 'I agree with',
                'terms2'    => 'terms and conditions'
            ];


            $text['blog'] = [
                'more' => 'More',
                'articles' => 'All articles'
            ];

            $text['contacts'] = [
                'street' => 'str. Cuza Vodă, 31/1 mun. Chișinău'
            ];

            /*HELP WITH MODALS*/
            $text['help_with_modals'] = [
                'free_consult' => [
                    'title' => 'Fast advice',
                    'intro' => 'We give free advice on various legal problems and issues:',
                    'sub_intro' => [
                        'on-line legal advice.',
                        'legal advice by telephone.',
                        'legal advice in our office.',
                    ],
                    'ending' => 'The requested legal advice on the problems of medium/advanced complexity will be given  for charge. In such situations we invite interested persons to our office for a complex analysis of their problems and identification of optimal solutions.'
                ],

                'public_service' => [
                    'title' => 'Public services',
                    'intro' => 'In the sphere of public services, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'Obtaining of civil status documents  (birth, marriage, divorce, etc).',
                        'registration and licensing of companies and other legal subjects.',
                        'registration of  imported vehicles; identification of vehicle; registration of transfer of the right of ownership; free use, rental, leasing.'
                    ],
                ],

                'business' => [
                    'title' => 'Business',
                    'intro' => 'In the sphere of business law, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'preparation, editing and expert evaluation of business contracts.',
                        'assistance in negotiations and transactions in the view of conclusion of business contracts.',
                        'disputes on  breach of contractual obligations, representation in front of the courts of common law and arbitration courts.',
                        'business companies – incorporation, counseling, applicable legal regime (registration, licensing, registration of amendments made to the incorporation documents of the company,  reorganization, suspension of activity, liquidation of legal entities).',
                        'collection of debts  – forced execution,   orders of payment, settlements.'
                    ]
                ],

                'business_assistence' => [
                    'title' => 'Assistance in the criminal cases in the sphere of business',
                    'intro' => 'It often happens that business projects or business plans risk to supersede the legal framework, so that their activity may contradict the criminal law. To prevent such situations, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'preparation of statements of case, petitions, claims, etc.',
                        'preparation of civil actions (collection of damage and pecuniary loss) in the criminal cases.',
                        'representation and assistance at the criminal prosecution bodies.',
                        'Filing objections against the actions of the criminal prosecution bodies.',
                        'contestation of actions and decisions of the  criminal prosecution bodies in the manner established by the law.',
                        'representation and assistance at the courts.',
                    ]
                ],

                'imobiles' => [
                    'title' => 'Real estate.',
                    'intro' => 'In the sphere of real estate,  we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'development of contracts, other legal acts on sale of real estate;lease (rent) of offices, apartments; rent of non-residential areas.',
                        'sub_list' => [
                            'title' => 'analysis, preparation of documents on the offer of:',
                            'sales of real estate.',
                            'lease (rent) of apartments, offices.',
                            'rent of non-residential areas.',
                        ],
                        'representation in the transactions of sale of real estate   (industrial real estate, residential houses, apartments, non-residential areas, construction lands,   arable lands, etc.',
                        'representation in the transactions of lease (rent) of  apartments, offices; rent of non-residential areas, etc.',
                        'representation at the law enforcement bodies and the courts on the real estate disputes.'
                    ]
                ],

                'debts' => [
                    'title' => 'Collection of debts (liabilities)',
                    'intro' => 'In the sphere of collection of debts (liabilities),  we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'loans not repaid on schedule (based on receipts, contracts of loan, etc.).',
                        'payment of payroll not paid by the employer within the  contractual term.',
                        'outstanding amounts of money based on the contracts of lease (rent).',
                        'outstanding amounts of money as a result of rendering the services.',
                        'outstanding amounts of money as a result of conclusion of transactions of sale and other types of transactions.',
                        'amounts of money paid by the Buyer (consumer) for improper  products, services.',
                    ]
                ],

                'avia' => [
                    'title' => 'Compensation of damage and assistance to the airline passengers',
                    'intro' => 'In this sphere, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'preparation of   inquiries, claims, orders of payment, statements of case, etc.',
                        'sub_list' => [
                            'title' => 'representation and legal assistance in front of:',
                            'national airlines.',
                            'state / private institutions and organizations in the Republc of Moldova.',
                            'law-enforcement bodies and courts.',
                        ],
                        'sub_list_2' => [
                            'title' => 'compensation of pecuniary and moral damage in connection with:',
                            'delay and cancelation of air flights.',
                            'loss and deterioration of baggage.',
                        ],
                    ]
                ],

                'transport' => [
                    'title' => 'Compensation of damage in case of traffic accidents.',
                    'intro' => 'In the sphere of compensation of damage for traffic accidents, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'preparation of   inquiries, claims, orders of payment, statements of case, etc.',
                        'sub_list' => [
                            'title' => 'representation and legal assistance in front of:',
                            'insurers of motor vehicles in the  Republic of Moldova.',
                            'National Center of Court Expertise under the auspices of the Ministry of Justice of the Republic of Moldova.',
                            'Independent centers of expertise  in the Republic of Moldova.',
                            'National Bureau of Vehicle Insurers of the   Republic of Moldova.',
                            'other state/ private institutions and organizations  in  the Republic of Moldova.',
                            'law enforcement bodies and courts.',
                        ],
                    ]
                ],

                'emigrants' => [
                    'title' => 'Compensation of damage and assistance in the labor of emigrants',
                    'intro' => 'In the sphere of labor migration we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'analysis of   job vacations abroad offered by the private employment agencies in the Republic of Moldova.',
                        'consulting and assistance upon conclusion of mediation contracts on employment abroad and of other legal documents.',
                        'collection of material, moral damage and pecuniary loss for breach of   contractual obligations by the private employment agencies in the Republic of Moldova.',
                        'Integration of family members in the society of the emigration country   (job, kindergarten, school, social services, medical services, cultural integration,   etc.).',
                    ]
                ],
            ];

            $text['activity_modals'] = [
                'business_right' => [
                    'title' => 'Business right',
                    'intro' => 'In the sphere of business law, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'preparation, editing and expert evaluation of business contracts.',
                        'assistance in negotiations and transactions in the view of conclusion of business contracts.',
                        'disputes on  breach of contractual obligations, representation in front of the courts of common law and arbitration courts.',
                        'business companies – incorporation, counseling, applicable legal regime (registration, licensing, registration of amendments made to the incorporation documents of the company,  reorganization, suspension of activity, liquidation of legal entities).',
                        'collection of debts  – forced execution,   orders of payment, settlements.'
                    ]
                ],

                'civil_right' => [
                    'title' => 'Civil law',
                    'intro' => 'In the sphere of civil law, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'preparation, editing and cessation of contracts.',
                        'assets division by court.',
                        'successions (transfer of rights and obligations).',
                        'deliсtual and contractual civil liability.',
                        'actions in nullity of transactions.',
                        'actions on acquisition of the right of ownership.',
                        'actions in the claims.',
                        'actions in the possessions.',
                        'succession, division of inherited assets, action on the entrance of an heir into their estate.',
                        'succession, division of inherited assets, action on the entrance of an heir into their estate.',
                        'legal assistance and representation at the court of first instance, Court of Appeal,  Supreme Court of Justice, Court of International Commercial Arbitration in Chisinau.',
                        'security measures.',
                        'assistance in the procedure of execution of court decisions.',
                        'assistance and representation at the pubic administrations, law enforcement bodies and notary public.  ',
                    ]
                ],

                'family_right' => [
                    'title' => 'Family law',
                    'intro' => 'In the sphere of family law, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'disputes.',
                        'divorce.',
                        'cancelation of marriage.',
                        'division of  assets of spouses.',
                        'deprivation of parental rights.',
                        'adoption.',
                        'establishment of maintenance pension.',
                        'current legal advice.',
                        'mediation of conflicts, settlement transaction.',
                        'legal assistance and representation in front of the notary public.',
                        'legal assistance and representation in front of the notary public.',
                        'legal assistance and representation in the relations with any individuals or legal entities.',
                    ]
                ],

                'work_right' => [
                    'title' => 'Labor and social protection law.',
                    'intro' => 'In the sphere of labor and social protection law, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'preparation, execution and cessation of employment contracts.',
                        'negotiation ofspecific clauses: clauses of  non-competition,   clauses of  confidentiality, clauses of mobility, etc.',
                        'assistance in the cessation of legal relationship of labor   (by resignation, dismissal, etc.). ',
                        'assistance in the labor disputes (conflicts) at the state/private institutions and courts.',
                    ]
                ],

                'intelectual_right' => [
                    'title' => 'Law of intellectual property',
                    'intro' => 'In the sphere of law of intellectual property, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'sub_list' => [
                            'title' => 'assistance and representation in connection with:',
                            'protection of intellectual property and copyrights.',
                            'conclusion of  specific contracts.',
                            'registration and legal protection of brand.',
                            'prevention and solution of the crimes of unfair competition and obtaining of damage.',
                            'civil  disputes and criminal cases on unfair competition, breach of copyrights and related rights.',
                        ]
                    ]
                ],

                'migrational_right' => [
                    'title' => 'Migration law',
                    'intro' => 'In the sphere of migration law, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'sub_list' => [
                            'title' => 'for emigrants  in the Republic of Moldova:',
                            'analysis of   job vacations abroad offered by the private employment agencies in the Republic of Moldova.',
                            'consulting and assistance upon conclusion of mediation contracts on employment abroad and of other legal documents.',
                            'collection of material, moral damage and pecuniary loss for breach of   contractual obligations by the private employment agencies in the Republic of Moldova.',
                            'integration of family members in the society of the emigration country (job, kindergarten, school, social services, medical services, cultural integration,   etc.).',
                        ],
                        'sub_list2' => [
                            'title' => 'for immigrants in the Republic of Moldova:',
                            'assistance in obtaining of temporary residence permit.',
                            'assistance in front of the Migration and Asylum Office  of the Ministry of Internal Affairs to obtain the residence permit.',
                            'assistance for employment.',
                            'assistance for enrollment to the studies.',
                            'assistance for enrollment to the studies.',
                            'assistance for starting up  a business within the territory of the  Republic of Moldova.',
                            'assistance for starting up  a business within the territory of the  Republic of Moldova.',
                        ],
                    ]
                ],

                'administrativ_right' => [
                    'title' => 'Administrative procedural law',
                    'intro' => 'In the sphere of administrative procedural law, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'cancelation ofillegal administrative acts made by the administrative bodies or subdivisions of a state administrative body.',
                        'participation in the  administrative procedural disputes and in front of the state institutions and courts.'
                    ]
                ],

                'penal_right' => [
                    'title' => 'Criminal law',
                    'intro' => 'In the sphere of criminal law, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'preparation of statements of case, petitions, claims, etc.',
                        'preparation of civil actions (collection of damage and pecuniary loss) in the criminal cases.',
                        'representation and assistance at the criminal prosecution bodies.',
                        'Filing objections against the actions of the criminal prosecution bodies.',
                        'contestation of actions and decisions of the criminal prosecution bodies in the manner established by the law.',
                        'representation and assistance at the courts.',
                    ]
                ],

                'execution_right' => [
                    'title' => 'Execution of court decisions',
                    'intro' => 'In the sphere of execution of court decisions, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'preparation of statements of case, motions, claims, etc.',
                        'collection of execution writs and initiation of execution procedure by the creditor’s application.',
                        'sub_list' => [
                            'title' => 'representation and legal assistance in front of:',
                            '-	Enforcement agents on the execution writs.',
                            '-	courts.',
                        ],
                        'Security of execution of the execution writ.',
                        'Withdrawal of enforcement agent.',
                        'Contestation of actions of enforcement agent.',
                        'Filing objections against the application, arguments and considerations of other parties to the execution procedure.',
                        'Search for a debtor and their assets.',
                        'Reconciliation of parties in the execution procedure and conclusion of the settlement transactions.',
                    ]
                ],

                'cedo_right' => [
                    'title' => 'European Court of Human Rights (ECHR)',
                    'intro' => 'In the sphere of ECHR, we give legal advice and perform representation on the following problems as follows:',
                    'sub_intro' => [
                        'analysis of court decisions and other legal acts.',
                        'Preparation and editing of applications to the European Court of Human Rights (ECHR), as well as other legal procedural acts.'
                    ]

                ]
            ];

            $text['tabs'] = [
                'tab1' => 'Pay online',
                'tab2' => 'Credit Cards',
                'tab3' => 'Self-service terminals',
                'tab4' => 'Pay by Cash',
                'desc2' => 'You can pay for our services through Visa or Mastercard bank cards.',
                'desc3' => 'Situated in general access locations.',
                'desc4' => 'Pay the services at the company office.',
                'cabinet' => 'Lawyer\'s office "Veaceslav Spalatu"',
                'adress'  => 'Mun. Chișinău, str. Cuza Vodă, 31/1, et. 3, of. nr. 8',
                'mcart_title' => 'VEACESLAV SPALATU',
                'mcart_numb' => '3102 1800 6051 7014',
                'mcart_acc' => '22592051520',
                'mcart_bank' => 'MAIB MasterCard',
                'qiwi' => 'qiwi: +37379998902'
            ];

        } else {

            $text['menu'] = [
                'home' => 'ГЛАВНАЯ',
                'help' => 'области специализации',
                'about_us' => 'О НАС',
                'domains' => 'ОБЛАСТИ ДЕЯТЕЛЬНОСТИ',
                'contacts' => 'КОНТАКТЫ',
                'blog'     => 'БЛОГ'
            ];

            $text['terms'] = [
                'title' => 'Политика сотрудничества',
                'content' => [
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Отношения сотрудничества между Вами, в качестве Бенефициара (Получателя), и Интернет порталом “avocat-moldova.md” регламентируют механизм предоставления профессиональной юридической консультации. Обращение, истребование и оплата услуг, предоставляемых Интернет порталом “avocat-moldova.md” является Вашим согласием с Правилами сотрудничества. Если Вы не хотите принимать Правила сотрудничества – не обращайтесь, не истребуйте и не оплачивайте услуги Интернет портала “avocat-moldova.md”. Настоящее соглашение о сотрудничестве применяется без дополнительного заверения в соответствии с законодательством Республики Молдова.</p>',
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Интернет портал “avocat-moldova.md” обязуется предоставить Бенефициару услуги профессиональной юридической помощи в соответствии с принятым и зарегистрированным заказом, а Бенефициар обязуется оплатить договорную цену в сроки и на условиях, предусмотренных в оферте (предложении).</p>',
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Оферта (предложение) будет содержать сроки исполнения и стоимость вознаграждения. Она может быть принята путем зачисления денег на счет Интернет портал “avocat-moldova.md” в срок, не превышающий 15 дней с момента выполнения второго шага процедуры сотрудничества (смотри процедуру сотрудничества).</p>',
                    '<p style="text-indent: 40px; text-align: justify; margin-bottom: 15px;">Отношения сотрудничества между Бенефициаром и Интернет порталом “avocat-moldova.md” регулируются следующей процедурой:</p>',
                    '<p style="text-indent: 40px; text-align: justify; padding-bottom: inherit;">Шаг 1. Бенефициар оформляет заказ на Интернет портале “avocat-moldova.md” путем детального описания существа проблемы и требований, направив документы на портал “avocat-moldova.md”, выбирает способ оплаты и срок, в который хочет получить ответ;</p>',
                    '<p style="text-indent: 40px; text-align: justify; padding-bottom: inherit;">Шаг 2. Бенефициар ожидает подтверждения заказа. На адрес Вашей электронной почты будет выслано письмо с указанием стоимости услуг, сроков и банковских реквизитов Интернет портала “avocat-moldova.md”;</p>',
                    '<p style="text-indent: 40px; text-align: justify; padding-bottom: inherit;">Шаг 3. Бенефициар оплачивает стоимость услуг и подтверждает оплату услуг письмом на электронный адрес интернет портала “avocat-moldova.md”. Мы начнем оказание услуг с момента получения оплаты аванса в размере 50 % от реальной стоимости консультации, оставшаяся часть должна будет быть оплачена одновременно с получением истребуемых документов или услуг;</p>',
                    '<p style="text-indent: 40px; text-align: justify;">Шаг 4. Готовые документы будут высланы на адрес Вашей электронной почты в запрашиваемый срок. Мы ответим немедленно на все Ваши вопросы в пределах оказанной услуги.</p>',
                ]
            ];

            $text['help_with'] = [
                'title' => 'МЫ ПОМОЖЕМ С',
                'title2' => 'Блог',
                1 => 'Быстрая консультация',
                2 => 'Государственные услуги',
                3 => 'Бизнес',
                4 => 'Помощь по уголовным делам <br/> в сфере бизнеса',
                5 => 'Недвижимость',
                6 => 'Взыскание долгов',
                7 => 'Компенсации ущерба и<br/>
    помощи Авиапассажирoв',
                8 => 'Возмещение ущерба при ДТП',
                9 => 'Возмещение ущерба и помощь в трудоустройстве еммигрантов',
				10 => 'Консультация',
            ];

            $text['deviza'] = [
                'Сражаемся вместе,',
                'побеждаем вместе'
            ];

            $text['about_us'] = [
                'title' => 'О НАС',
                'subtitle' => 'адвокат, доктор юридических наук',
                'description' => 'Cabinetul avocatului ,,Veaceslav Spalatu” este fondat în anul 2007 și activează în domeniul avocaturii mai bine de 10 ani. Cabinetul nostru de avocatură are menirea exclusivă de a vă ajuta în orice problemă juridică apărută; de a vă consulta, reprezenta, asista în chestiuni juridice de orice natură, în fața oricăror instituții, organizații, întreprinderi naționale și internaționale indiferent de forma de proprietate a lor; în fața organelor ale gestiunii administrative și de stat a Republicii Moldova și a altor state precum și în fața organelor de poliție, ale procuraturii, instanțelor de judecată, instituțiilor de executare naționale și din alte state, etc., cu toate împuternicirile reprezentantului prevăzute de legislația în vigoare a Republicii Moldova.',
                'principles' => [
                    'title' => 'ОСНОВНЫЕ ПРИНЦИПЫ',
                    'professionalism' => 'ПРОФЕССИОНАЛИЗМ',
                    'correct' => 'СПРАВЕДЛИВОСТЬ',
                    'trust' => 'ДОВЕРИЕ',
                    'punctuality' => 'ПУНКТУАЛЬНОСТЬ',
                    'responsability' => 'ОТВЕТСТВЕННОСТЬ',
                    'comunicability' => 'КОММУНИКАБЕЛЬНОСТЬ',
                ]
            ];

            $text['domains'] = [
                'title' => 'ОБЛАСТИ ДЕЯТЕЛЬНОСТИ',
                'Деловое право',
                'Гражданское право',
                'Семейное право',
                'Трудовое право и <br/> социальная защита',
                'Право возмещение <br/> интеллектуальной собственности',
                'Миграционное <br>право',
                'Право административного <br>судебного разбирательства',
                'Уголовное право',
                'Исполнение судебных <br>решений',
                'Европейский суд <br>по правам человека <br>(CEDO)',
            ];

            $text['titles'] = [
                'partners' => 'ПАРТНЕРЫ',
                'find_us' => 'Найти нас на карте',
                'contacts' => 'КОНТАКТЫ',
                'form_title' => 'Опишите вашу проблему',
                'form_subtitle' => 'и мы найдем решение!',
            ];

            $text['form'] = [
                'title' => 'Быстрая консультация',
                'title_pay' => 'Вы можете проконсультироваться онлайн нажав кнопку ниже',
                'name' => 'Имя',
                'phone' => 'Телефон',
                'problem' => 'Опишите проблему',
                'btn_consult' => 'Быстрая консультация',
                'btn_send' => 'Передать',
                'btn_pay'  => 'Оплатить сейчас!',
                'terms1'    => 'Я согласен с ',
                'terms2'    => '<br>Условиями использования'
            ];

            $text['blog'] = [
                'more' => 'Больше',
                'articles' => 'Все публикаций'
            ];

            $text['contacts'] = [
                'street' => 'Ул. Куза Водэ, 31/1, мун. Кишинёв,'
            ];

            /*HELP WITH MODALS*/

            $text['help_with_modals'] = [
                'free_consult' => [
                    'title' => 'Быстрая консультация',
                    'intro' => 'В отношении различных проблем и вопросов правого характера умеренной сложности мы предоставляем бесплатно:',
                    'sub_intro' => [
                        'юридическую консультацию онлайн.',
                        'юридическую консультацию по телефону.',
                        'юридическую консультацию в офисе.',
                    ],
                    'ending' => 'В отношении вопросов среднего/высокого уровня сложности, требуемая юридическая консультация будет представлена   платно. В таких ситуациях заинтересованные лица приглашаются в наш офис для комплексного анализа Ваших вопросов и нахождения оптимальных решений.'
                ],

                'public_service' => [
                    'title' => 'Публичные услуги',
                    'intro' => 'В области публичных услуг мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'получение документов гражданского состояния (рождение, брак, развод, т.д.).',
                        'регистрация и лицензирование компаний и других субъектов права.',
                        'регистрация импортированных транспортных средств; идентификация транспортных средств; регистрация передачи права собственности; договор безвозмездного пользования, найм, лизинг.'
                    ],
                ],

                'business' => [
                    'title' => 'Бизнес (экономическая деятельность)',
                    'intro' => 'В области экономического права мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'составление, редактирование и экспертиза хозяйственных договоров.',
                        'помощь в переговорах и сделках в целях заключения хозяйственных договоров.',
                        'споры в отношении нарушения договорных обязательств, представительство в судах общего права и в арбитражных судах.',
                        'Хозяйственные общества – учреждение, консультации, применяемый правовый режим (регистрация, лицензирование, регистрация изменений, вносимых к учредительным документам предприятия, реорганизация, приостановление деятельности, ликвидация юридических лиц).',
                        'истребование долгов – принудительное исполнение, платежные поручения, примирительные процедуры.'
                    ]
                ],

                'business_assistence' => [
                    'title' => 'Помощь по уголовным делам в области экономической деятельности',
                    'intro' => 'Зачастую есть риск того, что бизнес-планы или проекты некоторых компаний выйдут за рамки правового поля, вследствие чего их деятельность может противоречить уголовному праву. В целях предотвращения таких ситуаций мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'составление заявлений, ходатайств, жалоб, т.д.',
                        'составление гражданских исков (взыскание убытков и ущерба) по уголовным делам.',
                        'представление и помощь в органах уголовного преследования.',
                        'подача возражений против действий органов уголовного преследования.',
                        'опротестование действий и решений органов уголовного преследования в порядке, установленном законом.',
                        'представление и помощь в судебных инстанциях.',
                    ]
                ],

                'imobiles' => [
                    'title' => 'Недвижимое имущество',
                    'intro' => 'В области услуг в отношении недвижимого имущества мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'разработка договоров, других правовых актов в отношении купли-продажи недвижимого имущества; найма (аренды) квартир, офисов; аренды коммерческих помещений.',
                        'sub_list' => [
                            'title' => 'анализ, подготовка документации в отношении предложения:',
                            'по купли-продажи недвижимого имущества.',
                            'по найму (аренде) квартир, офисов.',
                            'по аренде коммерческих помещений.',
                        ],
                        'представление на сделках купли-продажи недвижимого имущества (промышленные объекты, жилые дома, квартиры, коммерческие помещения, офисы, участки под строительство, сельскохозяйственных земель, т.д.',
                        'представление на сделках по найму (аренде) квартир, офисов; аренде коммерческих помещений, т.д.',
                        'представление в правоохранительных органах и в судебных инстанциях по спорам в отношении недвижимого имущества.'
                    ]
                ],

                'debts' => [
                    'title' => 'Взыскание долгов (задолженностей)',
                    'intro' => 'В отношении взыскания долгов (задолженностей) мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'невозвращенные в срок займы (на основании расписок, договоров займа, т.д.).',
                        'взыскание заработной платы, невыплаченной работодателем в срок согласно договору.',
                        'денежные суммы, задолженные в результате договоров найма (аренды).',
                        'денежные суммы, задолженные в результате оказания услуг.',
                        'денежные суммы, задолженные в результате заключения сделок купли-продажи и других видов сделок.',
                        'денежные суммы, оплаченные Покупателем (потребителем) за несоответствующие товары, услуги.',
                    ]
                ],

                'avia' => [
                    'title' => 'Компенсация убытков и оказание помощи авиапассажирам',
                    'intro' => 'В данной области мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'составление   запросов, жалоб, требований об исполнении обязательств, исковых заявлений, т.д.',
                        'sub_list' => [
                            'title' => 'представление и правовая помощь перед:',
                            'национальными авиакомпаниями.',
                            'государственными/частными учреждениями и организациями Республики Молдова',
                            'правоохранительными органами и судебными инстанциями.',
                        ],
                        'sub_list_2' => [
                            'title' => 'Компенсация материального и морального ущерба в отношении:',
                            'задержки и отмены рейсов.',
                            'утери и повреждения багажа.',
                        ],
                    ]
                ],

                'transport' => [
                    'title' => 'Возмещение по дорожно-транспортным происшествиям',
                    'intro' => 'В отношении возмещений по дорожно-транспортным происшествиям мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'составление   запросов, жалоб, требований об исполнении обязательств, исковых заявлений, т.д.',
                        'sub_list' => [
                            'title' => 'представление и правовая помощь перед:',
                            'лицензированными автостраховщиками в Республике Молдова.',
                            'Национальным Центром Судебной Экспертизы при Министерстве Юстиции Республики Молдова.',
                            'Независимыми центрами экспертизы Республики Молдова.',
                            'Национальным Бюро Страховщиков Транспортных Средств Республики Молдова.',
                            'иными государственными/частными учреждениями и организациями Республики Молдова.',
                            'правоохранительными органами и судебными инстанциями.',
                        ],
                    ]
                ],

                'emigrants' => [
                    'title' => 'Компенсация убытков и оказание помощи в трудовых отношениях эмигрантам',
                    'intro' => 'В отношении трудовой миграции мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'анализ предложений по трудовым вакансиям за рубежом, предоставленных частными агентствами по трудоустройству в Республике Молдова.',
                        'консультация и помощь при заключении договоров посредничества в отношении трудоустройства за рубежом и иных правовых актов.',
                        'возмещение материального, морального ущерба, убытка за нарушение договорных обязательств частными агентствами по трудоустройству в Республике Молдова.',
                        'интеграция членов семьи в общество государства эмиграции (место работы, детский сад, школа, социальные услуги, медицинские услуги, культурная интеграция, т.д.).',
                    ]
                ],
            ];

            $text['activity_modals'] = [
                'business_right' => [
                    'title' => 'Деловое право',
                    'intro' => 'В области экономического права мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'составление, редактирование и экспертиза хозяйственных договоров.',
                        'помощь в переговорах и сделках в целях заключения хозяйственных договоров.',
                        'споры в отношении нарушения договорных обязательств, представительство в судах общего права и в арбитражных судах.',
                        'Хозяйственные общества – учреждение, консультации, применяемый правовый режим (регистрация, лицензирование, регистрация изменений, вносимых к учредительным документам предприятия, реорганизация, приостановление деятельности, ликвидация юридических лиц).',
                        'истребование долгов – принудительное исполнение, платежные поручения, примирительные процедуры.'
                    ]
                ],

                'civil_right' => [
                    'title' => 'Гражданское право',
                    'intro' => 'В области гражданского права мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:  ',
                    'sub_intro' => [
                        'составление, редактирование и прекращение договоров.',
                        'раздел по суду.',
                        'наследование <i>(передача прав и обязанностей)</i>.',
                        'договорная и деликтная гражданская ответственность.',
                        'действия при ничтожности актов.',
                        'действия в отношении приобретения права собственности.',
                        'действия в отношении требований.',
                        'действия в отношении владения.',
                        'наследство, раздел по наследству, действия по вступлению во владение наследуемым имуществом.',
                        'Составление и редактирование исковых заявлений, апелляционных, кассационных заявлений, уведомлений, требований, других требований, сделок, иных актов юридического характера.',
                        'правовая помощь и представительство перед судом первой инстанции, Апелляционным Судом, Высшей Судебной Палатой, Международном коммерческом арбитражном суде в г. Кишинёв.',
                        'исполнительные меры.',
                        'помощь в производстве исполнения судебных решений.',
                        'помощь и представительство перед государственными административными органами, правоохранительными органами и публичными нотариусом.',
                    ]
                ],

                'family_right' => [
                    'title' => 'Семейное право',
                    'intro' => 'В области семейного права мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'споры.',
                        'развод.',
                        'отмена брака.',
                        'раздел имущества супругов.',
                        'лишение родительских прав.',
                        'усыновление (удочерение).',
                        'назначение алиментов.',
                        'текущая правовая консультация.',
                        'посредничество при конфликтах, примирение.',
                        'правовая помощь и представительство перед публичным нотариусом.',
                        'правовая помощь и представительство в отношениях с государственными административными органами.',
                        'правовая помощь и представительство в отношениях с любыми физическими и юридическими лицами.',
                    ]
                ],

                'work_right' => [
                    'title' => 'Трудовое право и право социальной защиты',
                    'intro' => 'В области трудового права и права социальной защиты мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'составление, оформление и прекращение трудовых договоров.',
                        'ведение переговоров по особым условиям: условия не конкуренции, условия конфиденциальности, условия командировок, т.д.',
                        'помощь при прекращении трудовых правовых отношений <i>(в результате выхода в отставку, увольнений, т.д.)</i>. ',
                        'помощь в трудовых спорах <i>(конфликтах)</i> перед государственными/частными учреждениями и судебными инстанциями.',
                    ]
                ],

                'intelectual_right' => [
                    'title' => 'Право интеллектуальной собственности.',
                    'intro' => 'В области права интеллектуальной собственности мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'sub_list' => [
                            'title' => 'помощь и представительство в связи с:',
                            'защитой интеллектуальной собственности и авторских прав.',
                            'заключением особых договоров.',
                            'регистрацией и правовой защитой товарного знака.',
                            'предотвращением и борьбой с правонарушениями по нелояльной конкуренции и получением возмещений.',
                            'трудовыми спорами и уголовными делами в отношении нелояльной конкуренции, нарушением авторских прав и смежных прав.',
                        ]
                    ]
                ],

                'migrational_right' => [
                    'title' => 'Миграционное право',
                    'intro' => 'В области миграционного права мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'sub_list' => [
                            'title' => 'Для эмигрантов Республики Молдова:',
                            'анализ предложений по трудовым вакансиям за рубежом, предоставленных частными агентствами по трудоустройству в Республике Молдова.',
                            'консультация и помощь при заключении договоров посредничества в отношении трудоустройства за рубежом и иных правовых актов.',
                            'возмещение материального, морального ущерба, убытка за нарушение договорных обязательств частными агентствами по трудоустройству в Республике Молдова.',
                            'интеграция членов семьи в общество государства эмиграции (место работы, детский сад, школа, социальные услуги, медицинские услуги, культурная интеграция, т.д.).',
                        ],
                        'sub_list2' => [
                            'title' => 'Для иммигрантов в Республику Молдова:',
                            '-	помощь в получении временного вида на жительство.',
                            '-	помощь перед Бюро Миграции и Убежища МВД в целях получения вида на жительство.',
                            '-	помощь в трудоустройстве.',
                            '-	помощь в зачислении на учебу.',
                            '-	помощь в приобретении /аренде жилья.',
                            '-	помощь в открытии бизнеса на территории Республики Молдова.',
                            '-	помощь в целях временного получения номерного знака транспортных средств.',
                        ],
                    ]
                ],

                'administrativ_right' => [
                    'title' => 'Административное право',
                    'intro' => 'В области административного права мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'отмена незаконных административных актов, принятых административными органами или подразделениями государственного административного органа.',
                        'участие в спорах административного права перед государственными учреждениями и судебными инстанциями.'
                    ]
                ],

                'penal_right' => [
                    'title' => 'Уголовное право',
                    'intro' => 'В области уголовного права мы предоставляем юридическую консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'составление заявлений, ходатайств, жалоб, т.д.',
                        'составление гражданских исков (возмещение ущерба и убытков) по уголовным делам.',
                        'представление и помощь в органах уголовного преследования.',
                        'подача возражений против действий органов уголовного преследования.',
                        'опротестование действий и решений органов уголовного преследования в порядке, установленном законом.',
                        'представление и помощь в судебных инстанциях.',
                    ]
                ],

                'execution_right' => [
                    'title' => 'Исполнение судебных решений',
                    'intro' => 'В области исполнения судебных решений мы предоставляем консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'составление заявлений, ходатайств, жалоб, т.д.',
                        'получение исполнительных листов и возбуждение исполнительного производства по заявлению кредитора.',
                        'sub_list' => [
                            'title' => 'представительство и юридическая помощь перед:',
                            'судебными исполнителями в отношении исполнительных актов.',
                            'судебными инстанциями.',
                        ],
                        'обеспечение исполнения исполнительного акта.',
                        'отзыв судебного исполнителя.',
                        'опротестование актов судебного исполнителя.',
                        'подача возражений против заявлений, аргументов и соображений других участников в исполнительном производстве.',
                        'розыск должника и его имущества.',
                        'примирение сторон в исполнительном производстве и заключение мировых соглашений.',
                    ]
                ],

                'cedo_right' => [
                    'title' => 'Европейский Суд по Правам Человека (ЕСПЧ)',
                    'intro' => 'В области ЕСПЧ мы предоставляем консультацию и осуществляем представительство по следующим вопросам, а именно:',
                    'sub_intro' => [
                        'анализ судебных решений и других правовых актов.',
                        'составление и редактирование заявлений в адрес Европейского Суда по Правам Человека   (ЕСПЧ), а также других процессуальных актов.'
                    ]

                ]
            ];

            $text['tabs'] = [
                'tab1' => 'Оплата онлайн',
                'tab2' => 'Банковские карточки',
                'tab3' => 'Терминалы самообслуживания',
                'tab4' => 'Наличный способ оплаты',
                'desc2' => 'Вы можете оплачивать наши услуги через банковские карты Visa или Mastercard.',
                'desc3' => 'Терминалы QIWI предоставлены в местах с общим доступом.',
                'desc4' => 'Оплачивайте услуги непосредственно в офисе компании.',
                'cabinet' => 'Кабинет адвоката "Veaceslav Spalatu"',
                'adress'  => 'Мун. Кишинёв, ул. Куза Водэ, 31/1, эт. 3, оф. 8',
                'mcart_title' => 'VEACESLAV SPALATU',
                'mcart_numb' => '3102 1800 6051 7014',
                'mcart_acc' => '22592051520',
                'mcart_bank' => 'MAIB MasterCard',
                'qiwi' => 'qiwi: +37379998902'
            ];
        }

        return $text;
    }
}
