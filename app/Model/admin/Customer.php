<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = ['name', 'phone', 'status', 'transaction_id', 'problem',
        'ik_inv_id', 'ik_inv_st', 'ik_inv_no'];
}
