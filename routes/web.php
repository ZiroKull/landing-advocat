<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Redirect;

Auth::routes();

Route::get('/', 'FrontEndController@index');

Route::get('posts','BlogControllers@index');
Route::get('terms','TermsController@index');
Route::get('post/{slug}', ['as' => 'blog.single', 'uses' => 'BlogControllers@getSingle'] );

Route::get('/success', function(){
    return redirect()->action(
        'FrontEndController@index'
    );
});

Route::post('/success', 'PaymentsController@success');

//Admin Routes
Route::group(['namespace' => 'Admin'],function(){
    Route::get('admin/home','HomeController@index')->name('admin.home');

    Route::resource('admin/post','PostController');

    Route::get('/ajax_upload', 'PostController@index');
    Route::post('/ajax_upload/action', 'PostController@addPost')->name('ajaxupload.action');
    Route::post('/ajax_update/action', 'PostController@editPost')->name('ajaxupdate.action');
    Route::post('/ajax_get/action', 'PostController@getPost')->name('ajaxget.action');
    Route::post('/ajax_image/action', 'PostController@imagePost')->name('ajaximage.action');
    //

    Route::post('admin/addPost','PostController@addPost');
    Route::post('admin/editPost','PostController@editPost');
    Route::post('admin/deletePost','PostController@deletePost');

    Route::resource('admin/settings','SettingsController');
    Route::post('admin/editSettings','SettingsController@editSettings');

    Route::resource('admin/customers','CustomersController');

    // Admin Auth Routes
    Route::get('admin-login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('admin-login', 'Auth\LoginController@login');

    Route::get('admin-register', 'AuthController@showRegisterForm');
    Route::post('admin-make-register', 'AuthController@register');

    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
    // list all lfm routes here...
});

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');

    return Redirect::to('/');
});

Route::get('/cache', function() {
    Artisan::call('view:cache');
    Artisan::call('config:cache');

    return Redirect::to('/');
});

Route::get('/migrate', function() {
    Artisan::call('migrate');

    return Redirect::to('/');
});

Route::get('/home', 'HomeController@index')->name('home');